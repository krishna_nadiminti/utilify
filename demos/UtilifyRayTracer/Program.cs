﻿using System;
using System.Windows.Forms;
using Utilify.Framework;

namespace Utilify.Platform.Demo.UtilifyRayTracer
{
    static class Program
    {
        private static log4net.ILog log = null;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //just to initialize the logger
            log = log4net.LogManager.GetLogger(typeof(Program));
            
            //have a catch all on the AppDomain (which prevents the app from crash landing)
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            System.Windows.Forms.Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);

            //initialise the Utilify framework logger
            Logger.MessageLogged += new EventHandler<LogEventArgs>(Logger_MessageLogged);

            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
            System.Windows.Forms.Application.Run(new UtilifyRayTracer());
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            log.Warn(e.ExceptionObject.ToString());
            MessageBox.Show("Unexpected exception in AppDomain running RayTracer: " + e.ExceptionObject.ToString(),
                "RayTracer", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        internal static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            MessageBox.Show("Unexpected exception in Application running RayTracer: " + e.Exception.ToString(),
                "RayTracer", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //captures all framework log messages
        static void Logger_MessageLogged(object sender, LogEventArgs e)
        {
            string format = "<{0}:{1}> {2} - {3} {4}";
            switch (e.Level)
            {
                case LogLevel.Debug:
                    log.DebugFormat(format,
                        System.IO.Path.GetFileName(e.StackFrame.GetFileName()), e.StackFrame.GetFileLineNumber(), e.StackFrame.GetMethod().Name,
                        e.Message, e.Exception);
                    break;
                case LogLevel.Info:
                    log.InfoFormat(format,
                        System.IO.Path.GetFileName(e.StackFrame.GetFileName()), e.StackFrame.GetFileLineNumber(), e.StackFrame.GetMethod().Name,
                        e.Message, e.Exception);
                    break;
                case LogLevel.Warn:
                case LogLevel.Error:
                    log.ErrorFormat(format,
                        System.IO.Path.GetFileName(e.StackFrame.GetFileName()), e.StackFrame.GetFileLineNumber(), e.StackFrame.GetMethod().Name,
                        e.Message, e.Exception);
                    break;
            }
        }
    }
}
