using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading;
using Utilify.Platform;

namespace Utilify.Framework
{
    public partial class Application
    {
        //job table for holding/tracking Jobs
        private ClientJobTable jobTable = new ClientJobTable();

        //used for locking the application status property
        private object applicationStatusLock = new object();

        private const int SubmissionBatchSize = 15; //more MAGIC

        private Thread appDependencyResolver;
        private Thread jobMonitor;
        private Thread jobSubmitter;

        private object stopThreadsLock = new object();
        private bool stopThreads;

        private BackOffHelper backOff;
        private Logger logger;
        private EventWaitHandle jobsToSubmitWaitHandle = new AutoResetEvent(false);
        private EventWaitHandle jobsToMonitorWaitHandle = new AutoResetEvent(false);

        private bool StopThreads
        {
            get
            {
                bool stop = false;
                lock (stopThreadsLock)
                {
                    stop = stopThreads;
                }
                return stop;
            }
            set
            {
                lock (stopThreadsLock)
                {
                    stopThreads = value;
                }
            }
        }

        private void StartDependencyResolver()
        {
            appDependencyResolver = new Thread(new ThreadStart(SendApplicationDependencies));
            appDependencyResolver.Name = "AppDependencyResolver";
            appDependencyResolver.IsBackground = true;
            appDependencyResolver.Start();
        }

        private void StartJobMonitor()
        {
            //start off a new thread to monitor jobs and send locally queued jobs 
            jobMonitor = new Thread(new ThreadStart(MonitorJobs));
            jobMonitor.Name = "JobMonitor";
            jobMonitor.IsBackground = true;
            jobMonitor.Start();
        }

        private void StartJobSubmitter()
        {
            jobSubmitter = new Thread(new ThreadStart(SubmitJobs));
            jobSubmitter.Name = "JobSubmitter";
            jobSubmitter.IsBackground = true;
            jobSubmitter.Start();
        }

        private void StopAllThreads()
        {
            StopThreads = true;
            TimeSpan timeout = TimeSpan.FromMilliseconds(1000);
            jobsToSubmitWaitHandle.Set(); // Wake up JobSubmitter
            jobsToMonitorWaitHandle.Set(); // Wake up JobMonitor
            Helper.WaitAndAbort(appDependencyResolver, timeout);
            Helper.WaitAndAbort(jobSubmitter, timeout);
            Helper.WaitAndAbort(jobMonitor, timeout);

            //remove the references
            appDependencyResolver = null;
            jobSubmitter = null;
            jobMonitor = null;
        }

        //this is run on a seperate thread, after 'Start'
        private void SendApplicationDependencies()
        {
            try
            {
                SendDependenciesIfNeeded(this.Id, DependencyScope.Application, this.GetDependencies());
            }
            catch (ThreadAbortException) //handle an abort, since this method is called on a seperate thread
            {
                Thread.ResetAbort();
                logger.Debug("Application dependency resolver thread aborted");
            }
            catch (ObjectDisposedException ox)
            {
                logger.Debug("Object disposed when sending application dependencies: " + ox.ObjectName);
            }
            catch (Exception ex)
            {
                OnError(new PlatformException("Error sending dependencies for application", ex));
            }
            logger.Debug("Application dependency resolver thread ended.");
        }

        private void SubmitJobsRemote(Job[] jobs)
        {
            // First collect all JobSubmissionInfo's for the Jobs.
            List<JobSubmissionInfo> jobInfosToSubmit = new List<JobSubmissionInfo>();
            foreach (Job job in jobs)
            {
                //since this method is called by the Submitjobs and runs a potentially long loop, we check at each step
                if (StopThreads)
                    return;

                IExecutable executable = job.InitialInstance;

                //1. find dependencies and set them into the Job.
                List<DependencyInfo> deps = GetDependenciesToSubmit(executable);
                job.SetDependencies(deps);

                //2. find expected results to be sent to the Manager later.
                List<ResultInfo> res = GetResultsToSubmit(executable);

                //3. serialize job instance
                byte[] instance = SerializationHelper.Serialize(executable);

                //4. find job type
                JobType type;
                if (executable is NativeExecutable)
                    type = JobType.NativeModule;
                else
                    type = JobType.CodeModule;

                //5. add to list of jobs to send 
                JobSubmissionInfo jobInfo = new JobSubmissionInfo(this.id, type, instance, deps.ToArray(), res.ToArray());
                jobInfosToSubmit.Add(jobInfo);
            }

            //6. send jobs to remote manager
            //we expect to get the ids in the same order as the jobInfos submitted, which are in the same order as the jobs param
            //
            //we have two seperate loops: one for collecting all job infos, one for submitting. This is so that we batch remote calls
            //i.e reduce overheads - better performance
            //
            //note that cancelled Jobs are also submitted to the Manager and then they are cancelled in the same way other Jobs are cancelled.
            SendJobsToManager(jobInfosToSubmit, jobs);
        }

        private void SendJobsToManager(List<JobSubmissionInfo> jobInfosToSubmit, Job[] jobs)
        {
            bool submitted = false;
            String[] jobIds = new String[0];
            JobSubmissionException jx = null;

            while (!submitted)
            {
                if (StopThreads) //since we could be in this loop for a while - if we are not able to submit
                    return;

                try
                {
                    logger.Debug("Attempting to send {0} JobInfos", jobInfosToSubmit.Count);
                    jobIds = manager.SubmitJobs(jobInfosToSubmit.ToArray());
                    submitted = (jobIds != null && jobIds.Length == jobInfosToSubmit.Count);
                    logger.Debug("Successfully sent {0} JobInfos", jobInfosToSubmit.Count);
                    for (int i = 0; i < jobIds.Length; i++)
                    {
                        //not-initializing the jobStatus with a JobStatusInfo object here
                        //that will be automatically done in the next monitoring cycle.

                        //set the job id and initial Submitted status here - since it doesnt make sense to have an id but no status!
                        // we set the status here to Submitted so that these Jobs wont be re-submitted again.
                        Job remoteJob = jobs[i];
                        remoteJob.Id = jobIds[i];
                        jobTable.ChangeStatus(remoteJob, JobStatus.Submitted);
                    }

                }
                catch (Utilify.Platform.CommunicationException ex)
                {
                    logger.Warn("Failed to send JobInfos.", ex);
                    if (backOff.NumberOfRetries <= backOff.MaxRetries)
                        Thread.Sleep(backOff.GetNextInterval());
                    else
                    {
                        jx = new JobSubmissionException("Could not retrieve job ids from the server", ex);
                        break;
                    }
                }
            }
            logger.Debug("Submitted {0} jobs", jobIds.Length);
            
            //7. Get jobids and put these jobs in list of jobs to monitor
            // Note: We assume here that if submitted = true and jobIds != null, then ALL Jobs were submitted successfully.
            if (!submitted)
                OnError(jx);
        }

        private void SubmitDependenciesRemote(Job[] jobs)
        {
            foreach (Job job in jobs)
            {
                //since this method is called by the Submitjobs and runs a potentially long loop, we check at each step
                if (StopThreads)
                    return;

                if (!job.IsCancelRequested)
                {
                    // todo: See if this call to get dependencies is causing a lot of overhead. 
                    // Maybe return the dependency Ids during the initial job submission.
                    //now check if the job dependencies need to be sent
                    SendDependenciesIfNeeded(job.Id, DependencyScope.Job, job.GetDependencies());
                    //if the SendDependenciessIfNeeded didn't fail, we've sent it successfully or didn't have to send anything;
                    //update the job to make note of that.
                    job.SentDependencies = true; //no need to lock - since only this thread uses this property.
                }
            }
        }

        private List<ResultInfo> GetResultsToSubmit(IExecutable job)
        {
            List<ResultInfo> jobResults = new List<ResultInfo>();
            IResults resJob = job as IResults;
            if (resJob != null)
            {
                ResultInfo[] resultInfos = null;
                //cover for any exceptions thrown by the interface method: IResults.GetResults().
                try
                {
                    resultInfos = resJob.GetExpectedResults();
                }
                catch (Exception ex)
                {
                    logger.Warn("Could not get expected results for job. IResults.GetExpectdResults() threw an exception: ", ex);
                    OnError(ex);
                }
                if (resultInfos != null && resultInfos.Length > 0)
                {
                    //not calling res.AddRange here, since we want to check for null objects in the list
                    foreach (ResultInfo resInfo in resultInfos)
                    {
                        if (resInfo != null)
                            jobResults.Add(resInfo);
                    }
                }
            }
            return jobResults;
        }

        private List<DependencyInfo> GetDependenciesToSubmit(IExecutable executable)
        {
            logger.Debug("Getting dependencies to submit.");
            List<DependencyInfo> jobDependencies = new List<DependencyInfo>();

            //if the object implements this interface, then depJob won't be null
            IDependent depJob = executable as IDependent;
            if (depJob != null)
            {
                DependencyInfo[] interfaceDeps = null;
                //cover for any exceptions thrown by the interface method: IDependent.GetDependencies.
                try
                {
                    interfaceDeps = depJob.GetDependencies();
                    logger.Debug("Got {0} interface deps.", interfaceDeps.Length);
                }
                catch (Exception ex)
                {
                    logger.Warn("Could not get additional dependencies for job. IDependent.GetDependencies() threw an exception: ", ex);
                    OnError(ex);
                }
                if (interfaceDeps != null && interfaceDeps.Length > 0)
                {
                    //not calling depJobs.AddRange here, since we want to check each object for null
                    foreach (DependencyInfo depInfo in interfaceDeps)
                    {
                        if (depInfo != null) //just check for safety
                            jobDependencies.Add(depInfo);
                    }
                }
            }

            if (AutoDetectDependencies)
            {
                //1.1 auto-detect and add module dependencies
                List<DependencyInfo> moduleDeps = DependencyResolver.DetectDependencies(executable.GetType());
                foreach (DependencyInfo dep in moduleDeps)
                {
                    //if the module is found in the app's common dependencies or user-declared dependencies for this job :ignore it
                    //otherwise add it to the job's dependency list
                    if (!this.dependencies.Contains(dep) && !jobDependencies.Contains(dep))
                        jobDependencies.Add(dep);
                }
            }

            return jobDependencies;
        }

        /// <summary>
        /// Internal use only. This is called by SendApplicationDependencies / SubmitJobsRemote to send the dependencies for an app/job if needed
        /// </summary>
        /// <param name="appOrJobId"></param>
        /// <param name="scope"></param>
        /// <param name="dependencies"></param>
        private void SendDependenciesIfNeeded(string appOrJobId, DependencyScope scope, IEnumerable<DependencyInfo> dependencies)
        {
            Dictionary<string, DependencyInfo> depLookup = new Dictionary<string, DependencyInfo>();
            foreach (DependencyInfo dep in dependencies)
            {
                depLookup[dep.Hash] = dep; //need to keep the hashes - since we may not have ids yet for the deps.
            }
            logger.Debug("{0} dependencies in total for {1} '{2}'", depLookup.Count, scope, appOrJobId);
            if (depLookup.Count > 0)
            {
                DependencyInfo[] unresolvedDeps = manager.GetUnresolvedDependencies(new DependencyQueryInfo(appOrJobId, scope));
                if (unresolvedDeps != null && unresolvedDeps.Length > 0)
                {
                    foreach (DependencyInfo unresolvedDep in unresolvedDeps)
                    {
                        if (StopThreads) //just check since this could potentially take long
                            break;
                        if (depLookup.ContainsKey(unresolvedDep.Hash))
                        {
                            DependencyInfo localDepInfo = depLookup[unresolvedDep.Hash];
                            logger.Debug("Trying to send Dependency: {0}", localDepInfo.LocalPath);
                            //the unresolved deps we get from the manager will exclude the remote_url deps
                            SendDependencyRemote(unresolvedDep.Id, localDepInfo.LocalPath);
                        }
                        else
                        {
                            //raise an error event to tell the user
                            OnError(new PlatformException(
                                "Could not send dependencies. Server reported invalid file hash for dependency " + unresolvedDep));
                        }
                    }
                }
                else //if length is zero, that's good - meaning nothing to send!:)
                {
                    logger.Debug("All dependencies for {0} '{1}' are already resolved.", scope, appOrJobId);
                }
            }
            else // if depLookup count = 0, no declared dependencies - good : but rarely happens.
            {
                logger.Debug("No dependencies to send for {0} '{1}')", scope, appOrJobId);
            }
        }

        /// <summary>
        /// Sends an app/job's dependency to the manager
        /// </summary>
        private void SendDependencyRemote(string depId, string filePath)
        {
            //how do we stream this content? //todoLater: use DataTransfer service for files larger than 250KB or so.
            logger.Debug("Trying to read file (exists = " + System.IO.File.Exists(filePath) + "): " + System.IO.Path.GetFullPath(filePath));
            //If we fail here, lets do something nice.
            byte[] fileContent = IOHelper.ReadFile(filePath);

            DependencyContent content = new DependencyContent(depId, fileContent);

            bool sent = false;
            backOff.Reset();
            while (!sent)
            {
                try
                {
                    logger.Debug("Sending content for {0}.", filePath);
                    manager.SendDependency(content);
                    sent = true;
                    backOff.Reset();
                }
                catch (Utilify.Platform.CommunicationException ex)
                {
                    logger.Warn("Failed to send Content.", ex);
                    if (backOff.NumberOfRetries <= backOff.MaxRetries)
                        Thread.Sleep(backOff.GetNextInterval());
                    else
                        throw ex;
                }
            }
        }

        private void MonitorJobs()
        {
            const int loopWaitTime = 800; //milliseconds
            List<string> jobIds = new List<string>();
            
            // Job statuses (for easy lookup)
            //todoLater: Put this into the ClientJobTable too?..
            Dictionary<string, JobStatusInfo> jobStatusLookup = new Dictionary<string,JobStatusInfo>();
            try
            {
                while (!StopThreads)
                {
                    // Clear list from previous round of monitoring
                    jobIds.Clear();
                    // These are the JobIds to monitor this round.
                    jobIds.AddRange(jobTable.GetJobIds(StatusGroup.Active)); //make a copy - so there is no need to wait for the Submitter thread
                    // Also need to add the Submitted Jobs so we can pick up the Ready (and further) states
                    jobIds.AddRange(jobTable.GetJobIds(StatusGroup.Submitted));

                    if (jobIds.Count == 0) // is this synchronized with the other locks?
                    {
                        logger.Debug("*** Sleeping job monitor because we have no jobs to monitor.");
                        jobsToMonitorWaitHandle.WaitOne();
                        logger.Debug("*** Waking job monitor.");
                        if (StopThreads)
                            continue;
                    }

                    // query running jobs
                    try
                    {
                        logger.Debug("Got {0} jobs to monitor", jobIds.Count);
                        if (jobIds.Count > 0)
                        {
                            JobStatusInfo[] jobStatuses = manager.GetJobStatus(jobIds.ToArray());
                            if (jobStatuses != null)
                            {
                                foreach (JobStatusInfo newStatus in jobStatuses)
                                {
                                    //check
                                    if (newStatus != null && newStatus.ApplicationId == this.Id && jobIds.Contains(newStatus.JobId))
                                    {
                                        // No need to lock jobStatusLookup - since it is used only here in this thread

                                        // Get reference to the old StatusInfo for this Job.
                                        JobStatusInfo oldStatus = null;
                                        if (jobStatusLookup.ContainsKey(newStatus.JobId))
                                            oldStatus = jobStatusLookup[newStatus.JobId];
                                        
                                        // Update the StatusInfo entry for this Job.
                                        jobStatusLookup[newStatus.JobId] = newStatus;

                                        Job remoteJob = jobTable.GetJob(newStatus.JobId);

                                        // Handle Status Change. Raise Events to handle Job status changes.
                                        if (oldStatus != null && newStatus != null && !oldStatus.Status.Equals(newStatus.Status))
                                            HandleStatusChange(newStatus, remoteJob);
                                    }
                                }
                            }
                        }

                        DoCancelRequests();
                    }
                    catch (Utilify.Platform.CommunicationException ex)
                    {
                        OnError(new PlatformException("Error querying status of running jobs. Retrying...", ex));
                    }

                    jobsToMonitorWaitHandle.WaitOne(loopWaitTime, false);
                }
            }
            catch (ThreadAbortException)
            {
                Thread.ResetAbort();
                logger.Debug("MonitorJobs thread aborted.");
            }
            catch (ObjectDisposedException ox)
            {
                logger.Debug("Object disposed when monitoring jobs: " + ox.ObjectName);
            }

            logger.Debug("MonitorJobs thread ended.");
        }

        private void DoCancelRequests()
        {
            // Look for any jobs for which cancel has been requested while it was being submitted
            // and cancel them remotely
            Job[] activeJobs = jobTable.GetJobs(StatusGroup.Active);
            foreach (Job job in activeJobs)
            {
                lock (job)
                {
                    if (job.IsCancelRequested && !string.IsNullOrEmpty(job.Id))
                    {
                        manager.AbortJob(job.Id);
                        //in the next monitoring cycle the manager would report a CANCELLED status
                        job.IsCancelRequested = false; //since we've cancelled it
                    }
                }
            }
        }

        private void HandleStatusChange(JobStatusInfo newStatus, Job remoteJob)
        {
            // We need to set the status AFTER we have set all the other info because if someone
            // is checking for job completion using remoteJob.waitForCompletion() someone may try to access other information
            // before it is available.
            if (remoteJob.Status != newStatus.Status)
            {
                logger.Debug("Updating Job {0} status from {1} to {2}.", remoteJob.Id, remoteJob.Status, newStatus.Status);

                jobTable.ChangeStatus(remoteJob, newStatus.Status);

                switch (newStatus.Status)
                {
                    case JobStatus.Submitted:
                        remoteJob.SubmittedTime = newStatus.SubmittedTime;

                        //special case: extra event for submitted jobs.
                        OnJobSubmitted(remoteJob);
                        break;
                    case JobStatus.Scheduled:
                        remoteJob.ScheduledTime = newStatus.ScheduledTime;
                        break;
                    case JobStatus.Completed:
                    case JobStatus.Cancelled:

                        // Process Completed Jobs, before raising the completed event.
                        HandleCompletedJob(newStatus, remoteJob);
                        break;
                }

                //the status has changed : raise the event (executes user code, could take long-ish)
                //will be called on every status change, including Submitted and Completed.
                //this is an async call.
                OnJobStatusChanged(newStatus);

                //special case: extra event for completed/cancelled jobs.
                if (newStatus.Status == JobStatus.Completed ||
                    newStatus.Status == JobStatus.Cancelled)
                {
                    OnJobCompleted(remoteJob);
                }
            }
        }

        private void HandleCompletedJob(JobStatusInfo newStatus, Job remoteJob)
        {
            JobCompletionInfo info = manager.GetCompletedJob(remoteJob.Id);
            if (info != null)
            {
                logger.Info("Job {0} completed.", remoteJob.Id);
                IExecutable completedJob = null;
                Exception ex = null;
                try
                {
                    var jobInstance = info.GetJobInstance();
                    if (jobInstance != null)
                        completedJob = (IExecutable)SerializationHelper.Deserialize(info.GetJobInstance());
                }
                catch (SerializationException sx)
                {
                    OnError(sx);
                }

                try
                {
                    var exceptionInstance = info.GetJobException();
                    if (exceptionInstance != null)
                        ex = (Exception)SerializationHelper.Deserialize(info.GetJobException());
                }
                catch (SerializationException sx)
                {
                    //ignore this //OnError(sx);
                    logger.Warn("Could not deserialize job exception: ", sx);
                }

                if (completedJob != null)
                    remoteJob.CompletedInstance = completedJob;

                if (ex != null)
                    remoteJob.ExecutionException = ex;

                //we still need to set all the times here:
                //since we may not have got all the events in sequence.
                remoteJob.StartTime = info.StartTime;
                remoteJob.FinishTime = info.FinishTime;
                remoteJob.ScheduledTime = info.ScheduledTime;
                remoteJob.SubmittedTime = info.SubmittedTime;
                remoteJob.ExecutionHost = info.ExecutionHost;
                remoteJob.NumMappings = info.MappedCount;
                //CpuTime is in millis. TimeSpan takes ticks
                remoteJob.CpuTime = TimeSpan.FromMilliseconds(info.CpuTime);
                remoteJob.ClientFinishTime = DateTime.UtcNow;
            }
            else
            {
                OnError(new PlatformException(string.Format("Job {0} status is {1}, but could not retrieve completion info.",
                    newStatus.JobId, newStatus.Status)));
            }
        }

        private void SubmitJobs()
        {
            const int bufferTimeout = 250; //millis
            const int jobBufferSize = 3;
            const int maxBufferWaitLoops = 2;
            const int loopWaitTime = 2000;

            int bufferWaitLoops = 0;

            //submits jobs that are locally queued
            try
            {
                while (!StopThreads)
                {
                    bool submittedInThisRound = false;

                    // Get the NEW Jobs to Submit them to the Manager upto a max of SubmissionBatchSize.
                    Job[] jobsToSubmit = jobTable.GetJobs(StatusGroup.New, SubmissionBatchSize);                    
                    
                    if (jobsToSubmit.Length >= jobBufferSize ||
                        ((jobsToSubmit.Length > 0 && jobsToSubmit.Length < jobBufferSize) && bufferWaitLoops > maxBufferWaitLoops))
                    {
                        //can submit: either the buffer size is crossed or (we've waited enough and the job size is smaller than buffersize)!
                        try
                        {
                            //send these off
                            logger.Debug("Submitting {0} jobs. Looped {1} times. ", jobsToSubmit.Length,  bufferWaitLoops);

                            SubmitJobsRemote(jobsToSubmit);
                            submittedInThisRound = true;
                            bufferWaitLoops = 0; //reset wait loops
                        }
                        catch (Exception ex)
                        {
                            //something went wrong trying to serialize jobs / resolve dependencies
                            OnError(new JobSubmissionException("Error submitting jobs: could not send jobs or resolve dependencies", ex));
                        }
                    }
                    else
                    {
                        //the # of jobs is smaller than buffer size: and we haven't waited long enough to flush the buffer.
                        jobsToSubmitWaitHandle.WaitOne(bufferTimeout, false);
                        // increment the waitLoops because we are not submitting this round.
                        bufferWaitLoops++;
                    }

                    //Need to have this as a seperate block, since the job submission may succeed, but dependency sending may fail
                    try
                    {
                        Job[] jobsToSendDeps = jobTable.GetJobs(StatusGroup.SubmittedWaitingForDependencies);
                        // Send the Dependencies for any Jobs that have been Submitted
                        SubmitDependenciesRemote(jobsToSendDeps);
                    }
                    catch (Exception ex)
                    {
                        //something went wrong trying to send dependencies
                        OnError(new JobSubmissionException("Error submitting jobs: could not send dependencies", ex));
                    }

                    if (submittedInThisRound)
                    {
                        //sleep for loopWaitTime is finished or till someone wakes us up - whichever is earlier.
                        jobsToSubmitWaitHandle.WaitOne(loopWaitTime, false);
                    }
                }
            }
            catch (ThreadAbortException)
            {
                Thread.ResetAbort();
                logger.Debug("SubmitJobs thread aborted.");
            }
            catch (ObjectDisposedException ox)
            {
                logger.Debug("Object disposed when submitting jobs: " + ox.ObjectName);
            }
            
            logger.Info("SubmitJobs thread ended.");
        }

        /// <summary>
        /// Called internally, when the error event needs to be raised.
        /// </summary>
        /// <param name="error">The error.</param>
        protected virtual void OnError(Exception error)
        {
            logger.Warn("Raising OnError ", error);
            ErrorEventArgs args = new ErrorEventArgs(error);
            EventHelper.RaiseEvent<ErrorEventArgs>(Error, this, args);
        }

        /// <summary>
        /// Called internally, when a job status is changed.
        /// </summary>
        /// <param name="status">The status.</param>
        protected virtual void OnJobStatusChanged(JobStatusInfo status)
        {
            StatusChangedEventArgs args = new StatusChangedEventArgs(status);
            EventHelper.RaiseEventAsync<StatusChangedEventArgs>(JobStatusChanged, this, args);
        }

        /// <summary>
        /// Called internally, when a job is submitted.
        /// </summary>
        /// <param name="job">The job.</param>
        protected virtual void OnJobSubmitted(Job job)
        {
            JobSubmittedEventArgs args = new JobSubmittedEventArgs(job);
            EventHelper.RaiseEventAsync<JobSubmittedEventArgs>(JobSubmitted, this, args);
        }

        /// <summary>
        /// Called internally, when a job is completed.
        /// </summary>
        /// <param name="job">The job.</param>
        protected virtual void OnJobCompleted(Job job)
        {
            JobCompletedEventArgs args = new JobCompletedEventArgs(job);
            EventHelper.RaiseEventAsync<JobCompletedEventArgs>(JobCompleted, this, args);
        }
    }
}
