using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace Utilify.Platform.Executor
{
    internal partial class ExecutorService : ServiceBase
    {
        private const int MagicServiceTimeout = 28000; //just a bit less than the 30 secs for the Windows SCM

        private ExecutorHost host;

        public ExecutorService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //we create a new host each time, since the ctor for the host does some work (potentially time-consuming).            
            Thread executorStarter = new Thread(new ThreadStart(CreateAndStartHost));
            executorStarter.Name = "Executor Starter";
            executorStarter.Start();
        }

        protected override void OnStop()
        {
            if (host != null)
            {
                Thread executorStopper = new Thread(new ThreadStart(host.StopService));
                executorStopper.Name = "Executor Stopper";
                executorStopper.Start();
                executorStopper.Join(MagicServiceTimeout);

#if !MONO
                int timeout = 0;
                //try to get some more time upto another timeout period
                while (executorStopper.IsAlive && timeout <= MagicServiceTimeout)
                {
                    this.RequestAdditionalTime(500);
                    timeout += 500;
                }
#endif
                var disposable = (host as IDisposable);
                if (disposable != null)
                    disposable.Dispose();
                host = null; //remove reference to host
            }
        }

        private void CreateAndStartHost()
        {
            host = new ExecutorHost();
            host.OnShutDown += new EventHandler<EventArgs>(host_OnShutDown);
            host.StartService();
        }

        void host_OnShutDown(object sender, EventArgs e)
        {
            Stop();
        }
    }
}
