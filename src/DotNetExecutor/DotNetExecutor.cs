using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Utilify.Platform.Execution.Shared;
using Utilify.Platform.Executor;

// Sequence of handling the Job during out-of-process Execution:
// 
// DotNetExecutor -> ExecutionManager -> ClrExecutor
//
// DotNetExecutor: Listens for messages from JobDispatcher in main Executor process.
// ExecutionManager: Manages the execution of Jobs. Creates App Domains, makes calls to execute and abort Jobs.
// ClrExecutor: Implementation of the actual execution of the Job. Calls job.Execute().
// 
namespace Utilify.Platform.DotNetExecutor
{
    internal class DotNetExecutor
    {
        internal const int DefaultPort = 50210;
        private const string LocalMachine = "127.0.0.1";

	    private TcpClient client;
	    private Stream stream;
	    private ExecutionManager execManager;
        Logger logger = new Logger();

        // We should always be trying to use this constructor since we can't really assume a server exists on the DefaultPort
        public DotNetExecutor(int port)
        {
            execManager = new ExecutionManager();
		
		    // Connect back to the Executor
		    bool connected = false;
		    int retry = 0;

		    // Retry 3 times just incase we have some trouble. 
		    // Really it should connect the first time
		    // unless there is some issue.
		    while(!connected && retry < 3) 
            {
			    try 
                {
				    logger.Info("Connecting...");

                    IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(LocalMachine), port);
                    client = new TcpClient();
                    client.Connect(remoteEP);
				    stream = client.GetStream();

				    connected = true;
			    } 
                catch (SocketException se)
                {
                    logger.Warn(se.ToString());
                }

			    retry++;
    			
			    if (connected)
				    break;
    			
			    try 
                { 
                    Thread.Sleep(1000);
                } 
                catch (System.Threading.ThreadInterruptedException) {}
		    }

            logger.Info("Connected? " + connected);
        }

        internal void Start()
        {
            try
            {
                while (true)
                {
                    if (client == null || !client.Connected || stream == null)
                    {
                        logger.Warn("Could not connect to server data stream. Exiting...");
                        break;
                    }

                    MessageType msgType = MessageType.Unknown;
                    IMessageData msgData = MessageDataHelper.ReceiveMessage(stream);
                    if (msgData != null)
                        msgType = msgData.MessageType;
                    switch (msgType)
                    {
                        case MessageType.Unknown:
                            logger.Warn("Recvd unknown message type. Ignoring...");
                            break;

                        case MessageType.CompletedJobs:
                            logger.Warn("Illegal message: CompletedJobs is never supposed to be recvd by the DotNetExecutor. Ignoring...");
                            break;

                        case MessageType.ExecuteJob:
                            HandleExecuteJob(msgData);
                            break;

                        case MessageType.GetCompletedJobs:
                            HandleGetCompletedJobs(msgData);
                            break;

                        case MessageType.AbortJob:
                            HandleAbortJob(msgData);
                            break;

                        case MessageType.Shutdown:
                            HandleShutdown(msgData);
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                //log the message and die : nothing we can do here
                logger.Error(e.ToString());
            }
        }

        #region Command handlers

        private void HandleShutdown(IMessageData msgData)
        {
            //clean up here.
            if (stream != null)
            {
                stream.Close();
                stream = null;
            }
            if (client != null)
            {
                client.Close();
                client = null;
            }
        }

        private void HandleGetCompletedJobs(IMessageData msgData)
        {
            logger.Debug("Handling completed jobs");
            JobCompletionInfo[] completedJobs = GetCompletedJobs();
            logger.Debug("Got {0} completed jobs from ExecutionManager to return to Dispatcher", completedJobs.Length);
            JobCompletionMessageData jobCompletionData = new JobCompletionMessageData(completedJobs);
            logger.Debug("Created MessageData to send... {0} {1}", jobCompletionData.Infos.Length, jobCompletionData.MessageType);
            MessageDataHelper.SendMessage(stream, jobCompletionData);
            logger.Debug("Sent " + completedJobs.Length + " completed jobs");

            //delete the list of completed jobs that we just sent.
            RemoveCompletedJobs(completedJobs);
        }

        private void HandleAbortJob(IMessageData msgData)
        {
            logger.Debug("Handling AbortJob...");
            //we got the msg data: did we understand it?
            if (msgData.IsValid())
            {
                AbortJob(msgData as AbortJobMessageData);
            }
        }

        private void HandleExecuteJob(IMessageData msgData)
        {		    
            logger.Debug("Handling ExecuteJob...");

            if (msgData.IsValid())
            {
                ExecuteJob(msgData as ExecuteJobMessageData);
            }
        }

        #endregion

        #region Commands

        private void AbortJob(AbortJobMessageData msgData)
        {
            //Abort job
            string jobId = msgData.JobId;
            execManager.AbortJob(jobId);
        }

        private JobCompletionInfo[] GetCompletedJobs()
        {
            //Get completed jobs
            return execManager.GetCompletedJobs();
        }

        private void RemoveCompletedJobs(JobCompletionInfo[] completedJobs)
        {
            execManager.RemoveCompletedJobs(completedJobs);
        }

        private void ExecuteJob(ExecuteJobMessageData msgData)
        {
            // Execute job here
		    execManager.ExecuteJob(msgData.JobId,
				msgData.ApplicationId,
				msgData.JobDirectory,
				msgData.ApplicationDirectory,
				msgData.JobType,
				msgData.InitialJobInstance);
        }
        
        #endregion

    }
}
