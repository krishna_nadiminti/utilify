using Utilify.Platform.Contract;

namespace Utilify.Platform.Executor.InfoProvider
{
    public interface IPerformanceInfoProvider
    {
        PerformanceInfo GetPerformanceInfo(string id);

        ProcessorPerformanceInfo[] GetProcessorPerfInfo();

        DiskPerformanceInfo[] GetDiskPerfInfo();

        MemoryPerformanceInfo GetMemoryPerfInfo();
    }
}
