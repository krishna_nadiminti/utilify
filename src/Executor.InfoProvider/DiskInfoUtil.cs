using System;
using System.Collections.Generic;
using System.IO;

using Utilify.Platform.Contract;

namespace Utilify.Platform.Executor.InfoProvider
{
    internal static class DiskInfoUtil
    {
        private static List<DriveInfo> GetDrives()
        {
            List<DriveInfo> drives = new List<DriveInfo>();
#if !MONO
            drives.AddRange(DriveInfo.GetDrives());
#else
            //Note: Mono+Windows is not supported.
            //This doesn't work either:
            //string[] sDrives = Environment.GetLogicalDrives();
            //for (int i = 0; i < sDrives.Length; i++)
            //{
            //    drives.Add(new DriveInfo(sDrives[i]));
            //}
#endif
            return drives;
        }

        public static DiskSystemInfo[] GetDiskInfo()
        {
            List<DriveInfo> drives = GetDrives();
            List<DiskSystemInfo> tmp = new List<DiskSystemInfo>();

            foreach (DriveInfo drive in drives)
            {
                if (drive.DriveType.Equals(DriveType.Fixed) && drive.IsReady)
                    tmp.Add(new DiskSystemInfo(
                        drive.RootDirectory.ToString(),
                        (FileSystemType)Enum.Parse(typeof(FileSystemType), drive.DriveFormat, true),
                        drive.TotalSize));
            }

            return tmp.ToArray();
        }

        public static DiskPerformanceInfo[] GetDiskPerfInfo()
        {
            // Should only really check Drives that were identified earlier by GetDiskInfo()
            // Should we keep a local copy of the SystemInfo objects and pass them around when collecting perf. data?

            List<DriveInfo> drives = GetDrives();
            List<DiskPerformanceInfo> tmp = new List<DiskPerformanceInfo>();

            foreach (DriveInfo drive in drives)
            {
                if (drive.DriveType.Equals(DriveType.Fixed) && drive.IsReady)
                    tmp.Add(new DiskPerformanceInfo(
                        drive.RootDirectory.ToString(),
                        drive.TotalFreeSpace,
                        drive.TotalSize - drive.TotalFreeSpace)); 
                
                //Issue 056: should probably be reporting how much disk just this executor is using 
            }
            return tmp.ToArray();
        }
    }
}