using System.Net;
using Utilify.Platform.Contract;

namespace Utilify.Platform.Executor.InfoProvider.Win32
{
    internal class Win32SystemInfoProvider : ISystemInfoProvider
    {
        public SystemInfo GetSystemInfo(string id)
        {
            SystemInfo sysInfo =
                new SystemInfo(
                    id,
                    GetProcessorInfo(),
                    GetDiskInfo(),
                    GetMemoryInfo(),
                    GetOSInfo(),
                    Dns.GetHostName(),
                    true,
                    SystemInfo.DefaultPingInterval);

            return sysInfo;
        }

        private ProcessorInfoUtil processorInfoUtil;
        private ProcessorInfoUtil ProcessorInfoUtil
        {
            get
            {
                if (processorInfoUtil == null)
                    processorInfoUtil = new ProcessorInfoUtil();
                return processorInfoUtil;
            }
        }

        public ProcessorSystemInfo[] GetProcessorInfo()
        {
            return ProcessorInfoUtil.GetProcessorInfo();
        }

        public DiskSystemInfo[] GetDiskInfo()
        {
            return DiskInfoUtil.GetDiskInfo();
        }

        public MemorySystemInfo GetMemoryInfo()
        {
            return MemoryInfoUtil.GetMemoryInfo();
        }

        public OSInfo GetOSInfo()
        {
            return OsInfoUtil.GetOsInfo();
        }
    }
}
