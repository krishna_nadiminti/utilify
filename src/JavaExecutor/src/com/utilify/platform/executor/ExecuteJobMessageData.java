package com.utilify.platform.executor;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.utilify.framework.JobType;

public class ExecuteJobMessageData implements MessageData {
	
    private String jobId;
    private String applicationId;
    private String jobDirectory;
    private String applicationDirectory;
    private JobType jobType;
    private byte[] initialJobInstance;
	
//    public ExecuteJobMessageData(
//    		String jobId, 
//    		String applicationId, 
//    		String jobDirectory, 
//    		String applicationDirectory,
//    		String jobType,
//    		byte[] initialJobInstance) {
//    	
//    	this.jobId = jobId;
//    	this.applicationId = applicationId;
//    	this.jobDirectory = jobDirectory;
//    	this.applicationDirectory = applicationDirectory;
//    	this.jobType = JobType.fromValue(jobType);
//    	this.initialJobInstance = initialJobInstance;
//    }
    
    public ExecuteJobMessageData(byte[] bytes) {
    	try {
			ByteArrayInputStream stream = new ByteArrayInputStream(bytes);
			initialize(stream);
	        stream.close();
    	} catch (Exception e) {
			// some error.
    		e.printStackTrace();
    	}   	
    }
    
    public ExecuteJobMessageData(InputStream stream) {
    	initialize(stream);
    }

    private void initialize(InputStream stream) {
    	try {
            this.jobId = MessageDataHelper.readNextLine(stream);
            this.applicationId = MessageDataHelper.readNextLine(stream);
            this.jobDirectory = MessageDataHelper.readNextLine(stream);
            this.applicationDirectory = MessageDataHelper.readNextLine(stream);
            this.jobType = JobType.fromValue(MessageDataHelper.readNextLine(stream));
            this.initialJobInstance = MessageDataHelper.readNextBytes(stream);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
	public String getJobId() {
		return jobId;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public String getJobDirectory() {
		return jobDirectory;
	}

	public String getApplicationDirectory() {
		return applicationDirectory;
	}

	public JobType getJobType() {
		return jobType;
	}

	public byte[] getInitialJobInstance() {
		return initialJobInstance;
	}
	
	public String toString() {
		String strng = "";
		strng += ("jobId:"+this.jobId+"\n");
		strng += ("applicationId:"+this.applicationId+"\n");
		strng += ("jobDirectory:"+this.jobDirectory+"\n");
		strng += ("applicationDirectory:"+this.applicationDirectory+"\n");
		strng += ("jobType:"+this.jobType+"\n");
		return strng;
	}

	public boolean isValid() {
		boolean isValid = false;
		isValid = (jobId != null && jobId.trim().length() >0) &&
			(applicationId != null && applicationId.trim().length() >0) &&
			(jobDirectory != null && jobDirectory.trim().length() >0) &&
			(applicationDirectory != null && applicationDirectory.trim().length() >0);
		return isValid;
	}
}
