package com.utilify.platform.executor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MessageDataHelper {

	public static final String CHARSET = "UTF-8";
	private static final int INITIAL_BUFFER_SIZE = 10;
	
	//Ack and Nack should be the same byte count on the wire
	private static final String ACK = "ACK\n";	
	private static final String NACK = "NAK\n";
	
	private static byte[] readNextLineOfBytes(InputStream stream) throws IOException
    {
		int read = 0;
        byte[] data = new byte[INITIAL_BUFFER_SIZE];
        
        int nextByte = stream.read();
        
        while (nextByte != -1 && (((char)nextByte) != '\n' && ((char) nextByte) != '\r'))
        {
        	if (read == data.length) {
        		// resize array
        		byte[] newData = new byte[data.length * 2];
        		for(int i=0; i<data.length;i++)
        			newData[i] = data[i];
        		data = newData;
        	}
            data[read] = (byte)nextByte;
            nextByte = stream.read();
            read++;
        }

        // resize back to appropriate size
        byte[] newData = new byte[read];
        for (int i=0; i<read; i++)
        	newData[i] = data[i];
        
        return newData;
    }
	
    public static String readNextLine(InputStream stream) throws IOException 
    {
        byte[] bytes = readNextLineOfBytes(stream);
        String line = new String(bytes, CHARSET);
        System.out.println("read:" + line);
        return line;
    }

	public static byte[] readAllBytes(InputStream stream) throws IOException
    {
		int read = 0;
        byte[] data = new byte[INITIAL_BUFFER_SIZE];
        
        System.out.println("Reading all bytes...");
        int nextByte = stream.read();
        while (nextByte != -1)
        {
        	if (read == data.length) {
        		// resize array
        		byte[] newData = new byte[data.length * 2];
        		for(int i=0; i<data.length;i++)
        			newData[i] = data[i];
        		data = newData;
        	}
            data[read] = (byte)nextByte;
            nextByte = stream.read();
            read++;
        }
        
        // resize back to appropriate size
        byte[] newData = new byte[read];
        for (int i=0; i<read; i++)
        	newData[i] = data[i];
        
        return newData;
    }
	
	public static void writeAllBytes(OutputStream stream, byte[] bytes) throws IOException {
		stream.write(bytes);
    }
	
    public static void writeLine(OutputStream stream, String data) throws IOException {
        byte[] dataBytes = (data + "\n").getBytes(CHARSET);
        stream.write(dataBytes);
    }

	public static byte[] readNextBytes(InputStream stream) throws IOException {
        int numBytes = 0;
        String numBytesString = readNextLine(stream);
        try { numBytes = Integer.parseInt(numBytesString); } finally {}
        return readNextBytes(stream, numBytes);
	}
    
	public static byte[] readNextBytes(InputStream stream, int numBytes) throws IOException {
		if (numBytes <= 0)
			throw new IOException("Number of bytes to read cannot be <= 0");
		
        byte[] data = new byte[numBytes];
        stream.read(data);
        return data;
	}
	
	public static void writeNextBytes(OutputStream stream, byte[] bytes) throws IOException {
		if (bytes == null)
			bytes = new byte[0];
		
		writeLine(stream, "" + bytes.length);
		
		if (bytes.length > 0)
			writeAllBytes(stream, bytes);
	}
	
	public static void sendAck(OutputStream stream) throws IOException {
		byte[] ackData = ACK.getBytes(CHARSET);
		MessageDataHelper.writeAllBytes(stream, ackData);
	}

	public static void sendNack(OutputStream stream) throws IOException {
		byte[] nackData = NACK.getBytes(CHARSET);
		MessageDataHelper.writeAllBytes(stream, nackData);
	}

	public static boolean receiveAck(InputStream stream) throws IOException {
		byte[] origAckData = ACK.getBytes(CHARSET);
		byte[] ackOnStream = new byte[origAckData.length];
		stream.read(ackOnStream);
		String ack = (new String(ackOnStream, CHARSET));
		return ack.equals(ACK);
	}
}
