package com.utilify.platform.executor;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.utilify.framework.Helper;
import com.utilify.framework.JobType;
import com.utilify.framework.client.JobCompletionInfo;

/**
 */
public final class ExecutionManager implements ExecutionListener{
	
    private Map<String, URLClassLoader> classLoaders = new HashMap<String, URLClassLoader>();
    private Map<String, Object> executors = new HashMap<String, Object>();
    private ArrayList<JobCompletionInfo> completedJobs = new ArrayList<JobCompletionInfo>();
    private Map<String, Thread> executingThreads = new HashMap<String, Thread>();
    
    private Logger logger = Logger.getLogger(this.getClass().getName());

    public void executeJob(String jobId, String applicationId, String jobDirectory, 
        String applicationDirectory, JobType type, byte[] jobInstance) throws Exception {
    	
        logger.fine("Starting execution for job : " + jobId);

        //TODO: Discuss. Don't think we can just load libraries into the classloader for the application once.
        //      A new job coming in may define a library module which is copied to the app directory and needs to
        //      be loaded into the classloader.

        // Basicaly this code is always executing the first case in this IF statement since
        // the classLoaders list is never being added to. this works for now because if we keep the same
        // class loader for every job in an application AND we assume that jobs can be independent and have
        // independent sets of jar dependencies, then the class loader for the app would need to add these
        // new jars for each job.
        // We get a performance hit, but it works.
        ClassLoader cl = null;
        if (!classLoaders.containsKey(applicationId)){
        	//Do this only once per app (eventually)
        	
        	//1. get all the jar files in the app directory 
            File appDir = new File(applicationDirectory);
            File[] jarFiles = appDir.listFiles(new FileExtensionFilter(".jar"));
            ArrayList<URL> urls = new ArrayList<URL>();
            if (jarFiles != null){
            	for (File jarFile : jarFiles){
            		urls.add(jarFile.toURL());
            	}
            }
            
            // We also need to add the JvmExecutor.jar file here. We cant add it to the classpath
            // when starting the JavaExecutor from the main (.net) Executor because then it wont
            // be loaded into its own ClassLoader.
            urls.add(ExecutionHelper.getExecutionJarURL());
            
            //2. create a child classloader that will use those libs
            cl = new URLClassLoader(urls.toArray(new URL[urls.size()]));
            logger.fine("URL Class loader instance is " + cl);
            URL[] urlsTemp = ((URLClassLoader)cl).getURLs();
            for (URL url : urlsTemp)
            	logger.fine("Url in cl : " + url);            
        } else {
        	cl = classLoaders.get(applicationId);
        }
        
        //now, load the JvmExecutor into using the child classloader
        //and create a new instance
        
        Class executorClass = cl.loadClass(ExecutionHelper.getExecutorTypeName(type));
        Object executor = executorClass.newInstance();

        // Keep reference to Executor by JobId
        synchronized (executors) {
            executors.put(jobId, executor);
        }

        // Subscribe to Job finished  event from Executor.
        subscribeToCompletedEvent(executor);
        
        logger.fine("Running job in new thread.");

        // Start executing Job in new thread so that we can return to the client without timing out.
        JobExecutionInfo jobExecutionInfo = 
        	new JobExecutionInfo(jobId, applicationId, jobInstance, jobDirectory);
        
        Thread thread = new Thread(new JavaExecutorThread(executor, jobExecutionInfo));
        thread.setDaemon(true);
        thread.start();
        synchronized (executingThreads) {
            executingThreads.put(jobId, thread);
        }
    }

	private void subscribeToCompletedEvent(Object executor) 
		throws SecurityException, NoSuchMethodException, 
		IllegalArgumentException, IllegalAccessException, 
		InvocationTargetException {
		
		Method addListener = 
			executor.getClass().getMethod("addListener", new Class[] { ExecutionListener.class });
		
		addListener.invoke(executor, new Object[] { this });
	}
	
	private void unsubscribeFromCompletedEvent(Object executor) 
		throws SecurityException, NoSuchMethodException, 
		IllegalArgumentException, IllegalAccessException, 
		InvocationTargetException {
	
		Method removeListener = 
			executor.getClass().getMethod("removeListener", new Class[] { ExecutionListener.class });
		
		removeListener.invoke(executor, new Object[] { this });
	}

	public void abortJob(String jobId) {
        //don't throw an exception on invalid jobId

        // get the job thread.
        // kill the thread!
		// remove the thread
        synchronized (executingThreads)
        {
            Thread t = executingThreads.get(jobId);
            Helper.waitAndAbort(t, 1);
            executingThreads.remove(jobId);
        }
        synchronized (executors)
        {
            executors.remove(jobId);
        }
        // should we check completedJobs too?	
    }
	
	

	public JobCompletionInfo[] getCompletedJobs() {
        JobCompletionInfo[] infos = null;
        synchronized (completedJobs) {
        	infos = completedJobs.toArray(new JobCompletionInfo[completedJobs.size()]);
            completedJobs.clear();
        }
        return infos;
    }

	/*
	 * Event handler
	 * @see utilify.platform.test.ExecutionListener#executionComplete(utilify.platform.test.ExecutionCompleteEventData)
	 */
	public void executionComplete(Object executor, ExecutionCompleteEventArgs data) {
        try {
            logger.fine("Handling execution complete event for Job : " + data.getJobId());

            JobCompletionInfo info =
                new JobCompletionInfo(
                    data.getJobId(),
                    data.getApplicationId(),
                    data.getFinalJobInstance(),
                    data.getExecutionLog().toString(),
                    data.getExecutionError().toString());

            //unsubscribe from the event - so that the helper unsubscribes 
            unsubscribeFromCompletedEvent(executor);
            
            synchronized (executors){
                executors.remove(data.getJobId());
            }
            synchronized (completedJobs){
                completedJobs.add(info);
            }
            synchronized (executingThreads) {
            	executingThreads.remove(data.getJobId());
            }
        } catch (Exception ex) {
            logger.warning(ex.toString());
        }
	}
}
