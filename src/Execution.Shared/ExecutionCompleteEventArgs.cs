using System;

namespace Utilify.Platform.Execution.Shared
{
    [Serializable]
    public class ExecutionCompleteEventArgs : EventArgs
    {
        private String jobId;
        private String applicationId;
        private byte[] finalJobInstance;
        private StringAppender executionLog;
        private StringAppender executionError;
        private byte[] executionException;
        private long cpuTime;

        private ExecutionCompleteEventArgs()
            : base()
        { }

        public ExecutionCompleteEventArgs(String jobId, String applicationId, byte[] finalJobInstance,
            StringAppender executionLog, StringAppender executionError, byte[] executionException, long cpuTime)
            : base()
        {
            this.JobId = jobId;
            this.ApplicationId = applicationId;
            this.FinalJobInstance = finalJobInstance;
            this.ExecutionLog = executionLog;
            this.ExecutionError = executionError;
            this.ExecutionException = executionException;
            this.CpuTime = cpuTime;
        }

        public String JobId
        {
            get { return jobId; }
            private set 
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException("Value cannot be null or empty: jobId");
                jobId = value; 
            }
        }

        public String ApplicationId
        {
            get { return applicationId; }
            private set 
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException("Value cannot be null or empty: applicationId");

                applicationId = value; 
            }
        }

        public byte[] FinalJobInstance
        {
            get { return finalJobInstance; }
            private set { finalJobInstance = value; }
        }

        public StringAppender ExecutionLog
        {
            get { return executionLog; }
            private set { executionLog = value; }
        }

        public StringAppender ExecutionError
        {
            get { return executionError; }
            private set { executionError = value; }
        }

        public byte[] ExecutionException
        {
            get { return executionException; }
            private set { executionException = value; }
        }

        public long CpuTime
        {
            get { return cpuTime; }
            private set 
            {
                if (value < 0)
                    throw new ArgumentException("Value cannot be negative: cpuTime");
                cpuTime = value; 
            }
        }
    }
}
