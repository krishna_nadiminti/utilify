﻿using System;

namespace Utilify.Platform.Execution.Shared
{
    public interface IExecutor
    {
        event EventHandler<ExecutionCompleteEventArgs> ExecutionComplete;
        void ExecuteTask(object info);
    }
}
