package com.utilify.framework.dependency;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.utilify.framework.DependencyType;
import com.utilify.framework.ErrorMessage;
import com.utilify.framework.FrameworkException;
import com.utilify.framework.Helper;
import com.utilify.framework.client.DependencyInfo;

/**
 * Utility class used to resolve compile time dependencies of Java classes.
 * The <code>DependencyResolver</code> has methods to determine the list of classes / jars referenced by a specified class.
 */
public final class DependencyResolver {
	
	//cache of already loaded classes with references
	// Key (String) = class name, Value (JavaClass) = JavaClass instance with references
	private static HashMap<String, JavaClass> loadedClasses = new HashMap<String, JavaClass>();
		
	private static Logger logger = Logger.getLogger(DependencyResolver.class.getName());
	
	private static ArrayList<String> stdPackages = new ArrayList<String>();
	
	private static final String packageFilterConfigFile = "utilify_packages_exclude"; 
	
	static {

		stdPackages.add("com.utilify."); //by default exclude some packages
		stdPackages.add("javax.");
		stdPackages.add("java.");
		stdPackages.add("sun.");
		stdPackages.add("com.sun.");
		
		//load the standard packages from the packages.exclude file
		InputStream ins = null;
		BufferedReader reader = null;
		try {
			File file = new File(packageFilterConfigFile);
			if (file.exists())
				ins = new FileInputStream(file);
			else
				ins = DependencyResolver.class.getResourceAsStream("/" + packageFilterConfigFile);
			
			if (ins != null) {
				reader = new BufferedReader(new InputStreamReader(ins));
				
				String line = "";
				while (line != null){
					line = reader.readLine();
					if (line != null && !stdPackages.contains(line))
						stdPackages.add(line);
				}
			}
		} catch (IOException iox) {
			logger.warning("Could not find package filter config file. Continuing with defaults... " 
					+ Helper.getFullStackTrace(iox));
		} finally {
			try {
				if (reader != null)
					reader.close();
				if (ins != null)
					ins.close();

			} catch (IOException e) {
				logger.warning(Helper.getFullStackTrace(e));
			}
		}
	}
	
	/**
	 * Returns a collection of {@link DependencyInfo} objects that describe the dependencies for the class.
	 * The dependencies of the class are detected recursively till there are no more dependencies, or a detected dependency
	 * is explicitly ignore in configuration.
	 * @param clazz
	 * @return a collection of <code>DependencyInfo</code> objects.
	 * @throws IOException - if a file I/O error occurs while trying to determine the dependencies.
	 * @throws FrameworkException - if the location of a class could not be found or there was some other error detecting the dependencies.
	 * @throws IllegalArgumentException - if <code>clazz</code> is a null reference.
	 * @see ClassBuilder
	 */
	public static Collection<DependencyInfo> detectDependencies(Class<? extends Object> clazz) throws IOException, FrameworkException {
		if (clazz == null)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": clazz");
		
		// Key (string) = full file path, Value (DependencyInfo) = DependencyInfo object for the file
		HashMap<String, DependencyInfo> dependencies = new HashMap<String, DependencyInfo>();

		JavaClass jClass = null;
		synchronized (loadedClasses) {
			String className = clazz.getName();
			if (loadedClasses.containsKey(className)){
				jClass = loadedClasses.get(className);
			} else {
				//this will return us an entire JavaClass reference tree, 
				//- starting with the given class as the root of the tree,
				//- including all the classes referenced by each class, 
				//- excluding the classes in the given std-packages.
				ClassBuilder builder = new ClassBuilder(stdPackages);
				jClass = builder.buildReferenceTree(clazz);
				loadedClasses.put(className, jClass);
			}
		}
		
		//now we need a flat unique list of all the classes needed by the root class (including itself)
		//this will be the full list of the class dependencies
		
		Collection<JavaClass> referencedClasses = getFlatReferenceList(jClass);
		
		// File = class file, String = root directory for the class
		Map<File, String> classFiles = new HashMap<File, String>();
		ArrayList<File> jarFiles = new ArrayList<File>();
		for (JavaClass class1 : referencedClasses) {
			System.out.println("Flat ref: " + class1.getName() + ":\n\t" + class1.getSource().getCanonicalPath());
			File source = class1.getSource();
			if (source == null)
				throw new FrameworkException("Could not find source for class: " + class1.getName());
			if (class1.isInJar()){
				// the class exists inside 
				jarFiles.add(source);
			} else {
				//this must be its own class file.
				// store using the file and the class directory.
				classFiles.put(source, class1.getClassDirectory());
			}
		}
		
		for (File file : jarFiles) {
			String canonicalPath = file.getCanonicalPath();
			if (!dependencies.containsKey(canonicalPath)){
				DependencyInfo depInfo = new DependencyInfo(file.getName(), canonicalPath,
						DependencyType.JAVA_MODULE);
				dependencies.put(canonicalPath, depInfo);
			}
		}
		
		logger.fine("Got class files " + classFiles.size());
		
		//finally jar up all the class-files, and put them into the list of dependencies
		String jarFileName = String.format("%s-%d.jar", clazz.getSimpleName(), System.currentTimeMillis());
		if (classFiles.size() > 0){
			File jar = Helper.jarFiles(classFiles, jarFileName);
			String canonicalPath = jar.getCanonicalPath();
			DependencyInfo jarredDep = new DependencyInfo(jar.getName(), canonicalPath, 
					DependencyType.JAVA_MODULE);
			dependencies.put(canonicalPath, jarredDep);
		}
		
		return dependencies.values();
	}

	/**
	 * @param jClass
	 */
	private static Collection<JavaClass> getFlatReferenceList(JavaClass jClass) {
		//can be null if the initial class itself is filtered due to filtering rules
		if (jClass == null)
			return new ArrayList<JavaClass>();
		
		return jClass.getBuilder().getClassesBuilt();
		
//		ArrayList<JavaClass> classesChecked = new ArrayList<JavaClass>();
//		Stack<JavaClass> classesToCheck = new Stack<JavaClass>();
//		classesToCheck.push(jClass);
//		while (classesToCheck.size() > 0){
//			
//			JavaClass currentClass = classesToCheck.pop();
//			
//            if (classesChecked.contains(currentClass))
//            	continue;
//            
//            classesChecked.add(currentClass);
//            
//            String[] referencedClasses = currentClass.getReferencedClasses();
//            for (String referencedClass : referencedClasses){
//        		JavaClass referenced = builder.getJavaClass(referencedClass);
//        		if (referenced != null 
//        				&& !classesToCheck.contains(referenced)
//        				&& !classesChecked.contains(referenced))
//        			classesToCheck.push(referenced);
//            }
//		}
//		return classesChecked;
	}
	
	/**
	 * Gets the location of the class file for the specified class.
	 * The location of the class is determined from the @link {@link ProtectionDomain} of the 
	 * specified class. The returned location points to the path of the .class file or the .jar file containing
	 * the class file.
	 * @param clazz - the class whose location needs to be determined.
	 * @throws IllegalArgumentException - if <code>clazz</code> is a null reference.
	 * @return the location of the class file.
	 */
	public static String getLocation(Class<? extends Object> clazz) {
		if (clazz == null)
			throw new IllegalArgumentException("Class cannot be null: clazz");
		return ClassBuilder.getLocation(clazz);
	}
}
