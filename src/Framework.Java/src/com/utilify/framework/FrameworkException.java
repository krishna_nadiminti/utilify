
package com.utilify.framework;

import javax.xml.ws.WebFault;

/**
 * Thrown when an unexpected internal exception occurs in the framework.
 * The <code>FrameworkException</code> is the base class for most exceptions in the framework. 
 */
@WebFault(name = FrameworkFault.FaultName, targetNamespace = Namespaces.Framework)
public class FrameworkException extends Exception {

	private static final long serialVersionUID = 3651804473090501017L;
	
	/*
     * Java type that goes as soapenv:Fault detail element.
     */
    private FrameworkFault faultInfo;

    //needed for JAXWS to figure out if this is the wrapper class for a declared fault
    /**
     * Gets the SOAP fault that caused this exception.
     * @return the associated {@link FrameworkFault}.
     */
    public FrameworkFault getFaultInfo(){
    	return faultInfo;
    }

	/**
	 * Constructs a FrameworkException with no detail message.
	 */
	public FrameworkException() {
		super();
	}

	/**
	 * Constructs a FrameworkException with the specified detail message.
	 * @param message - the detail message.
	 */
	public FrameworkException(String message) {
		super(message);
	}

	/**
	 * Constructs a FrameworkException with the specified detail message and fault.
	 * @param message - the detail message.
	 * @param faultInfo - the fault object that is the cause of this exception.
	 */
	public FrameworkException(String message, FrameworkFault faultInfo) {
		super(message);
		this.faultInfo = faultInfo;
	}

	/**
	 * Constructs a FrameworkException with the specified detail message and cause.
	 * @param message - the detail message.
	 * @param cause - the cause of the exception.
	 */
	public FrameworkException(String message, Throwable cause) {
		super(message, cause);
	}
}