package com.utilify.framework;

import java.io.Serializable;

/**
 *
 */
public class ExecutionContext implements Serializable {

	private static final long serialVersionUID = 7048150165890959967L;

	private String jobId;
	private String workingDirectory;
	private StringAppender log;
	private StringAppender error;

	//prevent exposing a default-public ctor
	protected ExecutionContext() {
	}

	private ExecutionContext(String jobId, String workingDirectory,
			StringAppender log, StringAppender error) {

		setJobId(jobId);
		setWorkingDirectory(workingDirectory);
		setLog(log);
		setError(error);
	}

	/**
	 * @return the job Id
	 */
	public String getJobId() {
		return jobId;
	}

	private void setJobId(String value) {

		if (value == null || value.trim().length() == 0)
			throw new IllegalArgumentException("Job id cannot be empty");

		jobId = value;
	}

	/**
	 * @return the working directory
	 */
	public String getWorkingDirectory() {
		return workingDirectory;
	}

	private void setWorkingDirectory(String value) {

		if (value == null || value.trim().length() == 0)
			throw new IllegalArgumentException(
					"Working directory cannot be null or empty");

		workingDirectory = value;
	}

	/**
	 * @return the execution log
	 */
	public StringAppender getLog() {
		return log;
	}

	private void setLog(StringAppender value) {
		if (log == null)
			log = new StringAppender();
		else
			log = value;
	}

	/**
	 * @return the error log
	 */
	public StringAppender getError() {
		return error;
	}

	private void setError(StringAppender value) {
		if (error == null)
			error = new StringAppender();
		else
			error = value;
	}

	/**
	 * @param jobId
	 * @param workingDirectory
	 * @param log
	 * @param error
	 * @return the execution context
	 */
	public static ExecutionContext create(String jobId, String workingDirectory,
			StringAppender log, StringAppender error) {
		ExecutionContext context = new ExecutionContext(jobId,
				workingDirectory, log, error);
		return context;
	}
}
