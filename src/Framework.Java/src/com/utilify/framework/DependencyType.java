
package com.utilify.framework;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for DependencyType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DependencyType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DataFile"/>
 *     &lt;enumeration value="ClrModule"/>
 *     &lt;enumeration value="JavaModule"/>
 *     &lt;enumeration value="NativeModule"/>
 *     &lt;enumeration value="RemoteUrl"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
/**
 * Represents the type of a dependency. Dependencies can be of the following types:<br>
 * <li>DataFile</li><br>
 * Any file that is considered to be a data file that the user's application can use.
 * <li>ClrModule</li><br>
 * A .NET library or application module
 * <li>JavaModule</li><br>
 * A Java library or application (for example, a jar file or a class file/directory).
 * <li>NativeModule</li><br>
 * A native application or library (not written in a managed language such as Java or one of the .NET platform languages)
 * <li>RemoteUrl</li><br>
 * A url that points to a file or resource located on a remote server
 */
@XmlType(name = "DependencyType")
@XmlEnum
public enum DependencyType {

    /**
     * Dependency type for a data file.
     */
    @XmlEnumValue("DataFile")
    DATA_FILE("DataFile"),
    /**
     * Dependency type for a .NET CLR library or application.
     */
    @XmlEnumValue("ClrModule")
    CLR_MODULE("ClrModule"),
    /**
     * Dependency type for a Java library or application.
     */
    @XmlEnumValue("JavaModule")
    JAVA_MODULE("JavaModule"),
    /**
     * Dependency type for a native library or application.
     */
    @XmlEnumValue("NativeModule")
    NATIVE_MODULE("NativeModule"),
    /**
     * Dependency type for a remote resource.
     */
    @XmlEnumValue("RemoteUrl")
    REMOTE_URL("RemoteUrl");
    private final String value;

    DependencyType(String v) {
        value = v;
    }

    /**
     * Gets the value of the <code>DependencyType</code>
     * @return the value of the type (as a String).
     */
    public String value() {
        return value;
    }

    /**
     * Parses the specified value to create an <code>DependencyType</code> object.
     * @param v - the value from which to create the <code>DependencyType</code>.
     * @return the <code>DependencyType</code> object
     * @throws IllegalArgumentException - if the specified value is not one of the acceptable type values.
     */
    public static DependencyType fromValue(String v) {
        for (DependencyType c: DependencyType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
