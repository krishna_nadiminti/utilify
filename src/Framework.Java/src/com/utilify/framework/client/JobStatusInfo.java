
package com.utilify.framework.client;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.ErrorMessage;
import com.utilify.framework.JobStatus;

/*
 * <p>Java class for JobStatusInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="JobStatusInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplicationId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ExecutionHost" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FinishTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="JobId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Status" type="{http://schemas.utilify.com/2007/09/Framework}JobStatus"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Represents the status information for a job.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JobStatusInfo", propOrder = {
    "applicationId",
    "executionHost",
    "finishTime",
    "jobId",
    "startTime",
    "status"
})
public class JobStatusInfo {

    @XmlElement(name = "ApplicationId", required = true, nillable = true)
    private String applicationId;
    @XmlElement(name = "ExecutionHost", required = true, nillable = true)
    private String executionHost;
    @XmlElement(name = "FinishTime", required = true)
    @XmlSchemaType(name = "dateTime")
    private Date finishTime;
    @XmlElement(name = "JobId", required = true, nillable = true)
    private String jobId;
    @XmlElement(name = "StartTime", required = true)
    @XmlSchemaType(name = "dateTime")
    private Date startTime;
    @XmlElement(name = "Status", required = true)
    private JobStatus status;

    private JobStatusInfo(){}

	/**
     * Gets the application Id.
     * @return
     * 	the Id of the application for this job.    
     */
    public String getApplicationId() {
        return applicationId;
    }

    @SuppressWarnings("unused") //used by JAXWS
    private void setApplicationId(String value) {
		if (value == null || value.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": applicationId"); 
        this.applicationId = value;
    }

    /**
     * Gets the execution host name for this job.
     * If the job is not yet executing, it returns <code>null</code>
     * @return
     * 	the name of the host where the job executed.
     */
    public String getExecutionHost() {
        return executionHost;
    }

    @SuppressWarnings("unused") //used by JAXWS
	private void setExecutionHost(String value) {
        this.executionHost = value;
    }

    /**
     * Gets the finish time for this job.
     * If the job was not completed, it returns <code>null</code>
     * @return
     * 	the time when this job was completed.   
     */
    public Date getFinishTime() {
        return finishTime;
    }

    @SuppressWarnings("unused") //used by JAXWS
    private void setFinishTime(Date value) {
        this.finishTime = value;
    }

    /**
     * Gets the Id of this job.
     * 
     * @return 
     * 	 the job Id.   
     */
    public String getJobId() {
        return jobId;
    }

    @SuppressWarnings("unused") //used by JAXWS
    private void setJobId(String value) {
		if (value == null || value.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": jobId"); 
        this.jobId = value;
    }

    /**
     * Gets the start time of this job.
     * If the job is not yet started, it returns <code>null</code>.
     * @return  
     * 	the time when this job was started.
     */
    public Date getStartTime() {
        return startTime;
    }

    @SuppressWarnings("unused") //used by JAXWS
    private void setStartTime(Date value) {
        this.startTime = value;
    }

    /**
     * Gets the job status.
     * @return
     * 	the status of this job.
     */
    public JobStatus getStatus() {
        return status;
    }

    @SuppressWarnings("unused") //used by JAXWS
    private void setStatus(JobStatus value) {
    	if (value == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": status");
        this.status = value;
    }
}
