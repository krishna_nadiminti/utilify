
package com.utilify.framework.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.DependencyScope;
import com.utilify.framework.ErrorMessage;

/*
 * <p>Java class for DependencyQueryInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DependencyQueryInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplicationOrJobId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Scope" type="{http://schemas.utilify.com/2007/09/Framework}DependencyScope"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Represents the query information to find out about the unresolved dependencies for an application or a job.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DependencyQueryInfo", propOrder = {
    "applicationOrJobId",
    "scope"
})
public final class DependencyQueryInfo {

    @XmlElement(name = "ApplicationOrJobId", required = true, nillable = true)
    private String applicationOrJobId;
    @XmlElement(name = "Scope", required = true)
    private DependencyScope scope;

    private DependencyQueryInfo(){}
    
    /**
     * Creates an instance of the <code>DependencyQueryInfo</code> class with the specified application or job Id, and scope.
     * @param applicationOrJobId
     * 	the application or job Id to query.
     * @param scope
     * 	the scope of the dependency.
     * @throws IllegalArgumentException
     * 	if <code>applicationOrJobId</code> is <code>null</code> or empty, or <code>scope</code> is a <code>null</code> reference.
     */
    public DependencyQueryInfo(String applicationOrJobId, DependencyScope scope) {
    	setApplicationOrJobId(applicationOrJobId);
    	setScope(scope);
	}

	/**
     * Gets the application or job Id.
     * @return
     * the application or job Id that is being queried.    
     */
    public String getApplicationOrJobId() {
        return applicationOrJobId;
    }

    /**
     * Gets the scope of the dependency being queried. 
     * @return
     * 	the scope of the Id.  
     */
    public DependencyScope getScope() {
        return scope;
    }

    private void setApplicationOrJobId(String value){
    	if (value == null || value.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": applicationOrJobId");
    	this.applicationOrJobId = value;
    }
    
    private void setScope(DependencyScope value){
    	if (value == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": value");
    	this.scope = value;
    }
}
