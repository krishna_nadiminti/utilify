package com.utilify.framework.tests;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import com.utilify.framework.Helper;

public class TestHelper {

	private static Random gen = new Random();
	
	public static String getRandomTempFile() {
		int rand = gen.nextInt();
		File temp = null;
		try {
			temp = java.io.File.createTempFile("file" + rand, ".tmp");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (temp == null)
			return "randomFile";
		else
			return temp.getAbsolutePath();
	}

	public static byte[] getJobInstance(com.utilify.framework.client.tests.TestJob testJob) {
		byte[] instance = null;
		try {
			instance = Helper.serialize(testJob);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return instance;
	}

	public static byte[] getRandomBinaryContents() {
		return getJobInstance(new com.utilify.framework.client.tests.TestJob());
	}

	public static byte[] getSomeBytes() {
		byte[] bytes = new byte[255];
		gen.nextBytes(bytes);
		return bytes;
	}
}