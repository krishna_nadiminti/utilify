package com.utilify.framework.tests;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.sun.xml.bind.JAXBObject;
import com.utilify.framework.DependencyType;
import com.utilify.framework.FrameworkException;
import com.utilify.framework.client.DependencyInfo;
import com.utilify.framework.dependency.DependencyResolver;

public class DependencyResolverTest {
	
	private HashMap<String, DependencyInfo> actualList = new HashMap<String, DependencyInfo>();

	//TODO: discuss: directory hashes won't match when new zip files are created as in the tests below.
	//how do we test it for directories then?
	//for files it is easier
	
	@Before
	public void setup() throws MalformedURLException, IOException {
		//This won't pass the test: the dir is zipped twice to create different zip files. once here, and again once in the testMethod 
		//DependencyInfo dep = new DependencyInfo("bin", "bin", DependencyType.JAVA_MODULE);
		
		//this is ok : since it is a file
		DependencyInfo dep = new DependencyInfo("metroLib", "lib/webservices-rt.jar", DependencyType.JAVA_MODULE);
		
		actualList.put(dep.getHash(), dep);
		
		DependencyInfo dep1 = new DependencyInfo("metroLib", "lib/webservices-api.jar", DependencyType.JAVA_MODULE);
		actualList.put(dep1.getHash(), dep1);
	}

	@Test
	public void detectDependencies() throws IOException, FrameworkException {
		
		//todoLater: need to improve the speed of this test : print debugging statements to find out what's going on. 
		//this is ok : since it is a file
		Collection<DependencyInfo> dependencies = DependencyResolver.detectDependencies(javax.xml.ws.Service.class);
		HashMap<String, DependencyInfo> depMap = new HashMap<String, DependencyInfo>();
		for (DependencyInfo info : dependencies) {
			depMap.put(info.getHash(), info);
		}
		
		Assert.assertEquals("The size of detected dependencies should be the same as that of the actual list",
				actualList.size(), dependencies.size());
		
		for (DependencyInfo info : dependencies) {
			Assert.assertTrue("The infos detected should be in the actual list. The following info was not:" + info,
					actualList.containsKey(info.getHash()));
		}
		
		for (DependencyInfo info : actualList.values()) {
			Assert.assertTrue("The infos in the actual list should be detected. Couldn't find the following depInfo in detected list: "
					+ info,
					depMap.containsKey(info.getHash()));
		}		
	}
}