package com.utilify.framework.client.tests;

import org.junit.Test;

import com.utilify.framework.ResultRetrievalMode;
import com.utilify.framework.client.ResultInfo;
import com.utilify.framework.tests.TestHelper;

public class ResultInfoTest {

	/*
	 * [Category("ResultInfo")]
	 */
	
	@Test
	public void ResultInfoCtor() {
		new ResultInfo(TestHelper.getRandomTempFile(), ResultRetrievalMode.BACK_TO_ORIGIN);
		// no assertions - just to make sure the proper ctor works
	}

	@Test(expected = IllegalArgumentException.class)
	public void createResultInfoNullFileName() {
		new ResultInfo(null, ResultRetrievalMode.BACK_TO_ORIGIN);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createResultInfoEmptyFileName() {
		new ResultInfo(" ", ResultRetrievalMode.BACK_TO_ORIGIN);
	}
}