
package com.utilify.framework.client;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.ErrorMessage;

//todo: remove DependencyStatusInfo
/**
 * <p>Java class for DependencyStatusInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DependencyStatusInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Cached" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DependencyId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DependencyInfo" type="{http://schemas.utilify.com/2007/09/Client}DependencyInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DependencyStatusInfo", propOrder = {
    "cached",
    "dependencyId",
    "dependencyInfo"
})
public class DependencyStatusInfo {

    @XmlElement(name = "Cached", required = true)
    @XmlSchemaType(name = "dateTime")
    private Date cached;
    @XmlElement(name = "DependencyId", required = true, nillable = true)
    private String dependencyId;
    @XmlElement(name = "DependencyInfo", required = true, nillable = true)
    private DependencyInfo dependencyInfo;

    private DependencyStatusInfo(){}
    
    //no public ctor, since this class is created only on the server side
    
    /**
     * Gets the value of the cached property.
     * 
     * @return
     *     possible object is
     *     {@link Date }
     *     
     */
    public Date getCached() {
        return cached;
    }

    /**
     * Sets the value of the cached property.
     * 
     * @param value
     *     allowed object is
     *     {@link Date }
     *     
     */
    @SuppressWarnings("unused") //since this is set by JAXWS
	private void setCached(Date value) {
        this.cached = value;
    }

    /**
     * Gets the value of the dependencyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependencyId() {
        return dependencyId;
    }

    /**
     * Sets the value of the dependencyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @SuppressWarnings("unused") //since this is set by JAXWS
	private void setDependencyId(String value) {
    	if (value == null || value.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": dependencyId");
    		
        this.dependencyId = value;
    }

    /**
     * Gets the value of the dependencyInfo property.
     * 
     * @return
     *     possible object is
     *     {@link DependencyInfo }
     *     
     */
    public DependencyInfo getDependencyInfo() {
        return dependencyInfo;
    }

    /**
     * Sets the value of the dependencyInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link DependencyInfo }
     *     
     */
    @SuppressWarnings("unused") //since this is set by JAXWS
	private void setDependencyInfo(DependencyInfo value) {
        this.dependencyInfo = value;
    }

}
