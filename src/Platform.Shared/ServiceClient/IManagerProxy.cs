﻿using System.ServiceModel.Description;

namespace Utilify.Platform
{
    /// <summary>
    /// Specifies the contract for all client proxies connecting to services on the Manager.
    /// </summary>
    public interface IManagerProxy
    {
        /// <summary>
        /// Gets the client credentials.
        /// </summary>
        /// <value>The client credentials.</value>
        ClientCredentials ClientCredentials { get; }
        /// <summary>
        /// Gets the end point.
        /// </summary>
        /// <value>The end point.</value>
        ServiceEndpoint EndPoint { get; }
    }
}
