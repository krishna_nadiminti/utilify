using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform
{
    /// <summary>
    /// Represents the quality of service for an application submitted for distribution execution
    /// </summary>
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class QosInfo
    {
        private static QosInfo defaultQos
            = new QosInfo(0, Helper.MaxUtcDate, DateTime.UtcNow, Helper.MaxUtcDate, PriorityLevel.Normal);

        private int budget;
        private DateTime deadline;
        private DateTime earliestStartTime;
        private DateTime latestStartTime;
        private PriorityLevel priority;

        private QosInfo() { }

        /// <summary>
        /// Returns an instance of QosInfo set to its default values
        /// </summary>
        /// <returns>an instance of QosInfo</returns>
        public static QosInfo GetDefault()
        {
            return defaultQos;
        }

        /// <summary>
        /// Creates an instance of the QosInfo class with the specified parameters
        /// </summary>
        /// <param name="budget">budget that can be spent for execution</param>
        /// <param name="deadline">latest acceptable end time for the associated application</param>
        /// <param name="earliestStartTime">earliest allowable start time for the associated application</param>
        /// <param name="latestStartTime">latest acceptable start time for the associated application</param>
        /// <param name="priority">desired priority for the application</param>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// - budget is less than zero Or, <br/>
        /// - deadline is earlier than current UTC time
        /// </exception>
        public QosInfo(int budget, DateTime deadline, DateTime earliestStartTime, DateTime latestStartTime, PriorityLevel priority)
            : this()
        {
            this.Budget = budget;

            this.Deadline = Helper.MakeUtc(deadline);
            this.EarliestStartTime = Helper.MakeUtc(earliestStartTime);
            this.LatestStartTime = Helper.MakeUtc(latestStartTime);
            
            this.Priority = priority;
        }

        /// <summary>
        /// Gets the budget that can be spent for execution
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public int Budget
        {
            get { return budget; }
            private set 
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("budget", "Budget cannot be less than zero");
                budget = value; 
            }
        }

        /// <summary>
        /// Gets the date/time before which the application is required to complete execution
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DateTime Deadline
        {
            get { return deadline; }
            private set 
            {
                if (value < DateTime.UtcNow)
                    throw new ArgumentOutOfRangeException("deadline", "Deadline cannot be earlier than current UTC time");
                deadline = value;
            }
        }

        /// <summary>
        /// Gets the earliest allowable start time for the application this QosInfo instance is associated to.
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DateTime EarliestStartTime
        {
            get { return earliestStartTime; }
            private set { earliestStartTime = value; }
        }

        /// <summary>
        /// Gets the latest acceptable start time for the application this QosInfo instance is associated to.
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DateTime LatestStartTime
        {
            get { return latestStartTime; }
            private set { latestStartTime = value; }
        }

        /// <summary>
        /// Gets the priority for the application this QosInfo instance is associated to.
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public PriorityLevel Priority
        {
            get { return priority; }
            private set { priority = value; }
        }

        /// <summary>
        /// See <see cref="M:System.Object.Equals">System.Object.Equals()</see>
        /// </summary>
        /// <param name="other">the QosInfo object to compare this object to</param>
        /// <returns>true, if the specified object is equal to this object, false otherwise</returns>
        public override bool Equals(object other)
        {
            if (other == null || other.GetType() != this.GetType())
                return false;

            QosInfo otherQos = other as QosInfo;

            bool areEqual = 
                (otherQos.budget == this.budget) &&
                (otherQos.deadline == this.Deadline) &&
                (otherQos.earliestStartTime == this.earliestStartTime) &&
                (otherQos.latestStartTime == this.latestStartTime) &&
                (otherQos.priority == this.priority);
            
            return areEqual;
        }

        private int hashCode = 0;
        /// <summary>
        /// See <see cref="M:System.Object.GetHashCode">System.Object.GetHashCode</see>
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            if (hashCode == 0)
            {
                hashCode = 42 ^ earliestStartTime.GetHashCode() ^ latestStartTime.GetHashCode();
                if (budget != 0)
                    hashCode = budget ^ hashCode;
                if (priority != 0)
                    hashCode = priority.GetHashCode() ^ hashCode;
            }
            return hashCode;
        } //this object is not really intended to be used as a key in a hashtable, but its good to override GetHashCode when overriding Equals

        /// <summary>
        /// Gets a string representation of this object
        /// </summary>
        /// <returns>a string representation of the QosInfo instance</returns>
        public override string ToString()
        {
            return string.Format("QosInfo - Budget: {0}, Deadline: {1:r} {2}, EarliestStart: {3:r} {4}, LatestStart: {5:r} {6}, Priority: {7}",
                budget, deadline, deadline.Kind, earliestStartTime, earliestStartTime.Kind, latestStartTime, latestStartTime.Kind, priority);
        }
    }
}