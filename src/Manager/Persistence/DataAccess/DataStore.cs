using System;
using System.Collections.Generic;
using System.Data;
using NHibernate;
using NHibernate.Mapping;
//alias
using NHibernateConfig = NHibernate.Cfg.Configuration;

namespace Utilify.Platform.Manager.Persistence
{

    //todoLater: have a IValidatable interface implemented by all domain objects.
    //this can then be called by every Dao method to make sure the data is valid.
    //this is to provide in-depth validation of values to/from the db, in-case some malicious program/user injects 
    //values by-passing the Setters which do validation in general in most of our domain objects.
    //we can implement the CheckValid() such that it calls the setters again, with existing values which may end up throwing exceptions,
    //since they are meant to do the validation.

    /// <summary>
    /// Common data access methods in the DAL.
    /// </summary>
    internal sealed class DataStore : IDisposable //todoDoc
    {
        private ISessionFactory factory;
        private NHibernateConfig cfg;

        internal static long UnsavedVersionValue = -1;

        private static readonly Logger logger = new Logger();

        //Issue 030: perhaps we need locking here
        #region Static singleton methods
        private static DataStore singleton = null;
        public static DataStore GetInstance()
        {
            if (singleton == null)
                singleton = new DataStore();
            return singleton;
        }
        #endregion

        private DataStore()
        {
            try
            {
                cfg = new NHibernateConfig();                
                cfg.AddAssembly(this.GetType().Assembly);
                factory = cfg.BuildSessionFactory();
            }
            catch (NHibernate.HibernateException hx)
            {
                throw new PersistenceException("Error creating an instance of DataStore", hx);
            }
        }

        public ISession OpenSession()
        {
            VerifyDisposed();
            //factory is thread-safe, so this method is too : since we don't do anything else here           
            ISession session = null;
            try
            {
                session = factory.OpenSession();
            }
            catch (NHibernate.HibernateException hx)
            {
                throw new PersistenceException("Error opening session", hx);
            }
            return session;
        }

        /// <summary>
        /// Closes the session if it is not already closed.
        /// Safe to call multiple times on the same object, even if it is null.
        /// </summary>
        /// <param name="session"></param>
        public void CloseSession(ISession session)
        {
            VerifyDisposed();

            try
            {
                if (session != null && session.IsOpen)
                    session.Close();
            }
            catch (NHibernate.HibernateException hx)
            {
                throw new PersistenceException("Error closing session", hx);
            }
        }

        /// <summary>
        /// Disposes a transaction, if it is not already disposed.
        /// Safe to call multiple times on the same object, even if it is null.
        /// </summary>
        /// <param name="transaction"></param>
        public void CloseTransaction(ITransaction transaction)
        {
            if (transaction != null)
                transaction.Dispose();
            transaction = null;
        }

        /// <summary>
        /// Disposes a transaction, if it is not already disposed.
        /// Safe to call multiple times on the same object, even if it is null.
        /// </summary>
        /// <param name="transaction"></param>
        public void CloseTransaction(IDbTransaction transaction)
        {
            if (transaction != null)
                transaction.Dispose();

            transaction = null;
        }

        public T Get<T>(object id)
        {
            VerifyDisposed();

            if (id == null)
                throw new ArgumentNullException("id");

            ISession session = null;
            ITransaction transaction = null;
            T result = default(T);
            try
            {
                session = OpenSession();
                transaction = session.BeginTransaction();

                result = session.Get<T>(id, LockMode.None);
                
                transaction.Commit(); //commit, to signal end of transaction
            }
            catch (NHibernate.HibernateException hx)
            {
                RollbackAndDispose(transaction);
                transaction = null;

                CloseSession(session); //early dispose in case of an exception
                session = null;                

                throw new PersistenceException("Error getting object of type : " + typeof(T), hx);
            }
            finally
            {
                //double-check dispose:
                CloseTransaction(transaction);
                transaction = null;

                CloseSession(session);
                session = null;
            }
            return result;
        }

        public TAs GetAs<T, TAs>(object id) where T : ITransformable<TAs>
        {
            VerifyDisposed();

            if (id == null)
                throw new ArgumentNullException("id");

            ISession session = null;
            ITransaction transaction = null;
            TAs result = default(TAs);
            try
            {
                session = OpenSession();
                //need a transaction, since objects may be lazy loaded, and cause multiple queries.
                transaction = session.BeginTransaction();

                T domainObject = session.Get<T>(id, LockMode.None);

                if (domainObject != null)
                    result = (domainObject as ITransformable<TAs>).Transform();

                transaction.Commit(); //commit, to signal end of transaction
            }
            catch (NHibernate.HibernateException hx)
            {
                RollbackAndDispose(transaction);
                transaction = null;

                CloseSession(session); //early dispose in case of an exception
                session = null;

                throw new PersistenceException("Error getting object of type : " + typeof(T), hx);
            }
            finally
            {
                //double-check dispose:
                CloseTransaction(transaction);
                transaction = null;

                CloseSession(session);
                session = null;
            }
            return result;
        }

        public IList<T> GetList<T>()
        {
            //by default allow un-committed reads: so we have less db locking
            return GetList<T>(IsolationLevel.ReadUncommitted);
        }

        public IList<T> GetList<T>(IsolationLevel isolation)
        {
            VerifyDisposed();

            ISession session = null;
            ITransaction transaction = null;

            IList<T> results = new List<T>(); //just initialise an empty list
            try
            {
                session = factory.OpenSession();
                transaction = session.BeginTransaction(isolation);

                ICriteria criteria = session.CreateCriteria(typeof(T));
                results = criteria.List<T>();

                transaction.Commit(); //commit to signal end of transaction
            }
            catch (NHibernate.HibernateException hx)
            {
                RollbackAndDispose(transaction);
                transaction = null;

                CloseSession(session); //early close in case of an exception
                session = null;                

                throw new PersistenceException("Error getting list of objects of type : " + typeof(T), hx);
            }
            finally
            {
                //double-check dispose:
                CloseTransaction(transaction);
                transaction = null;

                CloseSession(session);
                session = null;
            }
            return results;
        }

        public List<TAs> GetListAs<T, TAs>(IsolationLevel isolation) where T : ITransformable<TAs>
        {
            VerifyDisposed();

            ISession session = null;
            ITransaction transaction = null;
            List<TAs> list = new List<TAs>();

            try
            {
                session = factory.OpenSession();
                transaction = session.BeginTransaction(isolation);

                ICriteria criteria = session.CreateCriteria(typeof(T));
                IList<T> results = criteria.List<T>();

                list = DataStore.Transform<T, TAs>(results);

                transaction.Commit(); //commit to signal end of transaction
            }
            catch (NHibernate.HibernateException hx)
            {
                RollbackAndDispose(transaction);
                transaction = null;

                CloseSession(session); //early close in case of an exception
                session = null;

                throw new PersistenceException("Error getting list of objects of type : " + typeof(T), hx);
            }
            finally
            {
                //double-check dispose:
                CloseTransaction(transaction);
                transaction = null;

                CloseSession(session);
                session = null;
            }
            return list;
        }

        /// <summary>
        /// Determines whether the object with the specified type and id exists in the database.
        /// Assumes that the object has a propertyName/field named 'id'
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ObjectExists(Type type, object id)
        {
            if (type == null)
                throw new ArgumentNullException("type");
            if (id == null)
                throw new ArgumentNullException("id");

            bool exists = false;
            ISession session = null;
            ITransaction tr = null;
            try
            {
                session = OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadCommitted);
                
                ICriteria criteria =
                    session.CreateCriteria(type)
                           .Add(NHibernate.Criterion.Expression.IdEq(id))
                           .SetProjection(NHibernate.Criterion.Projections.RowCountInt64());

                long count = (long)criteria.UniqueResult();
                tr.Commit(); //commit to signal end of the transaction
                exists = (count == 1);
            }
            catch (NHibernate.HibernateException hx)
            {
                RollbackAndDispose(tr);
                tr = null;

                CloseSession(session); //early close in case of exception
                session = null;

                throw new PersistenceException("Error checking object existence. Type : " + type + ", Guid : " + id, hx);
            }
            finally
            {
                //double check dispose
                CloseTransaction(tr);
                tr = null;

                CloseSession(session);
                session = null;
            }
            return exists;
        }

        public void Save(object objectToSave)
        {
            VerifyDisposed();

            if (objectToSave == null)
                throw new ArgumentNullException("objectToSave");

            ISession session = null;
            ITransaction transaction = null;
            try
            {
                session = OpenSession();
                transaction = session.BeginTransaction();
                session.SaveOrUpdate(objectToSave);
                transaction.Commit();
            }
            catch (NHibernate.HibernateException hx)
            {
                RollbackAndDispose(transaction);
                transaction = null;

                CloseSession(session); //early dispose in case of an exception
                session = null;

                throw new PersistenceException("Error saving object  : " + objectToSave, hx);
            }
            finally
            {
                CloseTransaction(transaction);
                transaction = null;

                CloseSession(session);
                session = null;
            }
        }

        //private string GetTableName(Type type)
        //{
        //    PersistentClass pc = cfg.GetClassMapping(type);
        //    if (pc == null)
        //        throw new MappingException("Could not find mapping for type " + type);
        //    return pc.Table.Name;
        //}

        #region IDisposable
        private bool disposed = false;

        //we are not using the full IDisposable pattern here since this is a sealed class
        void IDisposable.Dispose()
        {
            //Check to see if Dispose() has already been called
            if (disposed)
                return;

            if (factory != null)
                factory.Close();
 
            disposed = true;
        }

        //call verifydisposed in this class, wherever the object should be in a 'un'disposed state to work
        private void VerifyDisposed()
        {
            if (disposed)//verify in every method
            {
                throw new ObjectDisposedException(this.GetType().FullName);
            }
        }
        #endregion

        //Issue 031: should we abstract out IDataStore, and have a DataStore factory and add a new InMemoryDataStore / use SqlLite db in-memory?

        internal void RollbackAndDispose(ITransaction tr)
        {
            if (tr != null && tr.IsActive && !tr.WasRolledBack && !tr.WasCommitted)
            {
                try
                {
                    tr.Rollback();
                }
                catch (NHibernate.TransactionException tx) 
                {
                    //if there was no ADO transaction which can happen sometimes, 
                    //we can't really rollback - so we get this exception
                    //looks like we can't detect this beforehand though!
                    logger.Info("No transaction to rollback! {0}", tx);
                }
            }
                
            
            if (tr != null)
                tr.Dispose();

            tr = null;
        }

        internal void RollbackAndDispose(IDbTransaction tr)
        {
            if (tr != null)
                tr.Rollback();

            if (tr != null)
                tr.Dispose();

            tr = null;
        }

        internal void CheckConnection()
        {
            ISession session = null;
            try
            {
                session = OpenSession();
                if (session.Connection.State != System.Data.ConnectionState.Open)
                    session.Connection.Open();

                //IDbConnection cnn = session.Connection;
                //using (IDbCommand cmd = cnn.CreateCommand())
                //{
                //    cmd.Connection = cnn;
                //    cmd.CommandText = "Select 0"; //just a dummy query
                //    cmd.ExecuteNonQuery();
                //}
            }
            finally
            {
                CloseSession(session);
            }
        }

        public static List<T> Transform<S, T>(IEnumerable<S> sourceList) where S : ITransformable<T>
        {
            List<T> transferList = new List<T>();
            if (sourceList != null)
            {
                foreach (S sourceObj in sourceList)
                {
                    if (sourceObj != null)
                        transferList.Add(sourceObj.Transform());
                }
            }
            return transferList;
        }        
    }

    /// <summary>
    /// Specifies the fetch strategy for child objects of an object
    /// </summary>
    internal enum FetchStrategy
    {
        Default = 0,
        Eager = 1,
        Lazy = 2
    }

}
