using System;
using System.Collections.Generic;

using NHibernate;
using System.Data;
using Utilify.Platform.Contract;
using System.Text;

namespace Utilify.Platform.Manager.Persistence
{
    internal static class SystemInfoDao
    {
        internal static Guid RegisterExecutor(Executor executor)
        {
            if (executor == null)
                throw new ArgumentNullException("executor");

            // the Id should be empty here.
            if (executor.Id != Guid.Empty)
                throw new ArgumentException(
                    "The executor id must be empty. Re-registering an executor is not allowed.",
                    "executor");

            DataStore ds = DataStore.GetInstance();
            ds.Save(executor);

            return executor.Id;
        }

        internal static void UpdatePerformance(PerformanceInfo info)
        {
            ITransaction tr = null;
            ISession session = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction();

                Executor executor = session.Get<Executor>(Helper.GetGuidSafe(info.SystemId));
                executor.LastPingTime = DateTime.UtcNow;
                executor.UpdatePerformance(info);

                session.SaveOrUpdate(executor);

                tr.Commit();
            }
            catch (HibernateException ex)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException(string.Format("Error updating executor performance for executor {0}", info.SystemId), ex);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }
        }

        internal static void UpdateLimits(LimitInfo info)
        {
            ITransaction tr = null;
            ISession session = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction();

                Executor executor = ds.Get<Executor>(Helper.GetGuidSafe(info.SystemId));
                executor.LastPingTime = DateTime.UtcNow;
                executor.UpdateLimits(info);

                session.SaveOrUpdate(executor);

                tr.Commit();
            }
            catch (HibernateException ex)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException(string.Format("Error updating executor limits for executor {0}", info.SystemId), ex);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }
        }

        internal static Executor GetExecutor(Guid executorId, FetchStrategy fetchStrategy)
        {
            if (executorId == Guid.Empty)
                throw new ArgumentException(Messages.ArgumentEmpty, "executorId");

            DataStore ds = DataStore.GetInstance();
            ISession session = null;
            ITransaction tr = null;
            Executor exec = null;
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction();
                
                exec = session.Get<Executor>(executorId);
                if (fetchStrategy == FetchStrategy.Eager)
                {
                    //initialise the collections while the session is still open
                    Processor[] procs = exec.Processors;
                    Disk[] disks = exec.Disks;
                    Memory mem = exec.Memory;
                }
                tr.Commit();
            }
            catch (HibernateException ex)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException(string.Format("Error getting executor for id {0}", executorId), ex);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }
            return exec;
        }

        internal static bool ExecutorExists(Guid executorId)
        {
            //let's not throw an exception : we know this won't exist : so just say it isn't there!
            if (executorId == Guid.Empty)
                return false;

            DataStore ds = DataStore.GetInstance();
            bool exists = ds.ObjectExists(typeof(Executor), executorId);

            return exists;
        }

        internal static void UpdateLastPingTime(string id) //todoFix: fix to use ORM
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentException("Argument cannot be null or empty", "id");

            DataStore ds = DataStore.GetInstance();
            ISession session = null;
            IDbTransaction trans = null;
            try
            {
                session = ds.OpenSession();
                IDbConnection conn = session.Connection;
                trans = conn.BeginTransaction(IsolationLevel.Serializable);
                
                IDbCommand cmd = conn.CreateCommand();
                cmd.CommandText = "update Executor set lastPingTime=@lastPingTime where id=@id;";

                IDbDataParameter p1 = cmd.CreateParameter();
                p1.ParameterName ="@lastPingTime";
                p1.Value = DateTime.UtcNow.Ticks;
                cmd.Parameters.Add(p1);

                IDbDataParameter p2 = cmd.CreateParameter();
                p2.ParameterName = "@id";
                p2.Value = id;
                cmd.Parameters.Add(p2);

                cmd.Transaction = trans;
                cmd.ExecuteNonQuery();

                trans.Commit();
            }
            catch (HibernateException ex)
            {
                //early release
                ds.RollbackAndDispose(trans);
                trans = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error updating last ping time", ex);
            }
            finally
            {
                ds.CloseTransaction(trans);
                trans = null;
                ds.CloseSession(session);
                session = null;
            }
        }

        /// <summary>
        /// Gets a list of executors which have (mapped jobs + executing jobs) &lt; # of CPUs
        /// sorted by # of free slots (# of CPUs - (mapped + executing jobs)) in descending order,
        /// that has a more recent last-connected time than the specified value
        /// </summary>
        /// <param name="lastPingTime">the date when the executor last pinged the manager</param>
        /// <returns></returns>
        internal static IList<Executor> GetFreeExecutorsSortedByAvailableSlots(DateTime lastPingTime)
        {
            ISession session = null;
            DataStore ds = DataStore.GetInstance();
            ITransaction tr = null;
            List<Executor> execs = new List<Executor>();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadCommitted); //read committed stuff here

                StringBuilder hql = new StringBuilder();

                //Need two seperate queries, since otherwise we can't do a having clause.
                //and without a having clause we can't do the required count(cpus) aggregation.
                //need to use exec.processors as a left join with an  alias or else NHib 2.0 seems to be generating incorrect sql
                hql.AppendLine("Select exec.Id From Executor exec ");
                hql.AppendLine("    Left Join exec.processors as cpu ");
                hql.AppendLine(" Where exec.LastPingTimeTicks >= :lastPingTimeTicks ");
                hql.AppendLine(" Group by exec.Id ");
                hql.AppendLine(" Having (2 * count(cpu) > exec.JobStatistics.ActiveJobs) ");
                hql.AppendLine(" Order by (count(cpu) - exec.JobStatistics.ActiveJobs) Desc ");

                IQuery query = session.CreateQuery(hql.ToString());
                query.SetInt64("lastPingTimeTicks", lastPingTime.Ticks);                
                IList<Guid> execIds = query.List<Guid>();

                if (execIds != null && execIds.Count > 0)
                {
                    //todo: (GetFreeExecutorsSortedByAvailableSlots()) there is a potential limit here: # of parameters (executor Ids)
                    //sql allowed a limited # of parameters. NHib creates one param per value in the list
                    hql.Remove(0, hql.Length); //clear it out
                    hql.AppendLine(
                        @"Select exec From Executor exec
                            Where exec.Id In (:idList)");
                    query = session.CreateQuery(hql.ToString()); //do another query

                    //have to make it a list, since SetParameterList needs a ICollection
                    List<Guid> ids = new List<Guid>();
                    ids.AddRange(execIds);
                    query.SetParameterList("idList", ids);

                    execs.AddRange(query.List<Executor>());

                    foreach (Executor exec in execs)
                    {
                        //just to make sure the processors collection gets initialised here
                        //before we leave the session
                        Processor[] procs = exec.Processors;
                    }
                }

                tr.Commit(); //commit to signal end of transaction
            }
            catch (NHibernate.HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting executors by free slots.", hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }
            return execs;
        }

        /// <summary>
        /// Gets the entire list of exectors - and their static properties.
        /// </summary>
        /// <returns></returns>
        internal static List<SystemInfo> GetExecutors()
        {
            DataStore ds = DataStore.GetInstance();
            return ds.GetListAs<Executor, SystemInfo>(IsolationLevel.ReadUncommitted);            
        }

        internal static List<PerformanceInfo> GetExecutorPerformance()
        {
            DataStore ds = DataStore.GetInstance();
            return ds.GetListAs<Executor, PerformanceInfo>(IsolationLevel.ReadUncommitted);
        }

        internal static void Update(Guid execId, SystemInfo info)
        {
            if (info == null)
                throw new ArgumentNullException("info");

            if (execId == Guid.Empty)
                throw new ArgumentException("The executor id cannot be empty", "execId");

            if (!ExecutorExists(execId))
                throw new ExecutorNotFoundException(Messages.ExecutorNotFound, execId.ToString());

            DataStore ds = DataStore.GetInstance();
            ISession session = null;
            ITransaction tr = null;
            Executor exec = null;
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction();

                exec = session.Get<Executor>(execId);
                exec.UpdateSystem(info); // Update with new info in case system has changed
                exec.LastPingTime = DateTime.UtcNow;

                tr.Commit();
            }
            catch (HibernateException ex)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException(string.Format("Error updating executor for id {0}", execId), ex);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }
        }

        internal static Executor RefreshExecutor(Executor executor)
        {
            if (executor == null)
                throw new ArgumentNullException("executor");

            DataStore ds = DataStore.GetInstance();
            ISession session = null;
            ITransaction tr = null;
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction();

                session.Lock(executor, LockMode.Read);

                //initialise the executor's processor collection:
                Processor[] procs = executor.Processors;

                tr.Commit();
            }
            catch (HibernateException ex)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException(string.Format("Error refreshing executor for id {0}", executor.Id), ex);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }

            return executor;
        }
    }
}
