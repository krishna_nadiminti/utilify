using System;
using System.Collections.Generic;

using NHibernate;
using System.Data;
using Utilify.Platform.Contract;
using Utilify.Platform.Manager.Security;
using System.Net;

namespace Utilify.Platform.Manager.Persistence
{
    internal static class SecurityDao
    {
        private static Logger logger = new Logger();

        private const string UserQueriesRegion = "UserQueries";
        private const string OwnershipQueriesRegion = "EntityOwnershipQueries";
        
        internal static bool AuthenticateUser(string username, string password)
        {
            if (username == null || username.Trim().Length == 0)
                return false;
            if (password == null || password.Trim().Length == 0)
                return false;

            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            bool authenticated = false;

            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadCommitted); //read committed stuff here

                string hql = @"Select count(usr.Name) From User usr 
                                 Where usr.Name = :username 
                                 And usr.Password = :password";

                IQuery query = session.CreateQuery(hql);
                query.SetCacheable(true);
                query.SetCacheRegion(UserQueriesRegion);

                query.SetString("username", username);
                query.SetString("password", password);

                long count = (long)query.UniqueResult();
                if (count == 1)
                    authenticated = true;

                tr.Commit(); //commit to signal end of transaction
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Could not authenticate user: " + username, hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }

            return authenticated;
        }

        internal static List<Utilify.Platform.Manager.Security.Permission> GetUserPermissions(string username)
        {
            if (username == null)
                throw new ArgumentNullException("username");
            if (username.Trim().Length == 0)
                throw new ArgumentException(Messages.ArgumentEmpty, "username");

            IList<Permission> permissions;
            List<Utilify.Platform.Manager.Security.Permission> permissionEnums = new List<Utilify.Platform.Manager.Security.Permission>();
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadUncommitted);

                //we don't want left joins here.
                //basically we want results only if permissions exist for this group
                string hql = @"Select perm From User as usr 
                                Join usr.Group as grp 
                                Join grp.permissions as perm
                               Where usr.Name = :username";
                
                IQuery query = session.CreateQuery(hql);
                query.SetString("username", username);
                query.SetCacheable(true);
                query.SetCacheRegion(UserQueriesRegion);

                permissions = query.List<Permission>();
               
                if (permissions != null)
                {
                    foreach (Permission permission in permissions)
                    {
                        permissionEnums.Add((Utilify.Platform.Manager.Security.Permission)permission.Id);
                    }
                }

                tr.Commit(); //commit to signal end of transaction
            }
            catch (NHibernate.HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting permissions for user: " + username, hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }

            return permissionEnums;
        }

        // If a single windows account is mapped to multiple internal usernames then when we try to map the windows
        // account to an internal user we wont know which one to choose.
        /// <summary>
        /// Gets the internal username for the given username.
        /// Currently each internal username is mapped to only one windows account and/or one x509 cert.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="idType"></param>
        /// <returns></returns>
        internal static string GetInternalUsername(string username, IdentityType idType)
        {
            if (username == null)
                throw new ArgumentNullException("username");
            if (username.Trim().Length == 0)
                throw new ArgumentException("Username cannot be empty", "username");
            if (idType == IdentityType.Unknown)
                throw new ArgumentException("Identity type cannot be 'Unknown'", "idType");

            // At the moment the Username/Password username is what is used for the internal username so just return.
            if (idType == IdentityType.GenericIdentity)
                return username;

            string internalUsername = string.Empty;
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                string hql = "Select usr.Name From User usr ";
                switch (idType)
                {
                    case IdentityType.WindowsIdentity:
                        hql += " Where usr.WindowsUsername = :username";
                        break;
                    case IdentityType.X509Identity:
                        hql += " Where usr.CertificateName = :username";
                        break;
                    default: //should never happen: unless we put in a new IdentityType in the enum and *dont* use it here
                        return null; //dont' query if we don't know the identity type
                }

                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadCommitted); //read committed stuff here
                IQuery query = session.CreateQuery(hql);

                //internal user name does not change that often. so should be ok to cache.
                query.SetCacheable(true);
                query.SetCacheRegion(UserQueriesRegion);

                query.SetString("username", username);
                internalUsername = query.UniqueResult<string>();

                tr.Commit(); //commit to signal end of transaction
                
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Could not get mapped user name: " + username, hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }

            return internalUsername;
        }
        
        internal static User GetUser(string username)
        {
            if (username == null)
                throw new ArgumentNullException("username");
            if (username.Trim().Length == 0)
                throw new ArgumentException("User name cannot be empty", "username");

            DataStore ds = DataStore.GetInstance();
            return ds.Get<User>(username);
        }

        internal static IList<User> GetUsers()
        {
            DataStore ds = DataStore.GetInstance();
            return ds.GetList<User>();
        }

        internal static IList<Group> GetGroups()
        {
            DataStore ds = DataStore.GetInstance();
            return ds.GetList<Group>();
        }

        //currently, not used anywhere: (kna, 4 Oct 08)
        //internal static IList<Permission> GetPermissions()
        //{
        //    DataStore ds = DataStore.GetInstance();
        //    return ds.GetList<Permission>();
        //}

        internal static void UpdateUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            if (String.IsNullOrEmpty(user.Name))
                throw new ArgumentException("The user name cannot be empty", "User.Name");

            if (!UserExists(user.Name))
                throw new ArgumentException("Could not update User with specified name", user.Name);

            DataStore ds = DataStore.GetInstance();
            ds.Save(user);
        }

        internal static void CreateUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            if (UserExists(user.Name))
                throw new ArgumentException(Messages.UserAlreadyExists, user.Name);

            logger.Debug("****** Saving user : " + user.Name + " " + user.WindowsUsername + " " + user.CertificateName + " " + user.Group.Name);
            
            DataStore ds = DataStore.GetInstance();
            ISession session = null;
            ITransaction tr = null;
            try
            {
                session = ds.OpenSession();

                tr = session.BeginTransaction();
                Group group = session.Get<Group>(user.Group.Id);
                if (group == null)
                    throw new GroupNotFoundException(Messages.GroupNotFound, user.Group.Id);
                user.Group = group;
                session.Save(user);
                tr.Commit();
            }
            catch (NHibernate.HibernateException hx)
            {
                ds.RollbackAndDispose(tr);

                throw new PersistenceException("Error creating user.", hx);
            }
            finally
            {
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }
        }
        
        internal static bool UserExists(string username)
        {
            //let's not throw an exception : we know this won't exist : so just say it isn't there!
            if (username == null || username.Trim().Length == 0)
                return false;

            DataStore ds = DataStore.GetInstance();
            bool exists = ds.ObjectExists(typeof(User), username);
            return exists;
        }

        /// <summary>
        /// Gets a value specifying whether the given user owns the given entity.
        /// It does not matter if a user has admin permissions. Ownership is seperate from permissions.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <returns></returns>
        internal static bool IsOwner(string username, string entityId, EntityType entityType)
        {
            if (username == null)
                throw new ArgumentNullException("username");
            if (username.Trim().Length == 0)
                throw new ArgumentException(Messages.ArgumentEmpty, "username");
            if (entityId == null)
                throw new ArgumentNullException("entityId");
            if (entityId.Trim().Length == 0)
                throw new ArgumentException(Messages.ArgumentEmpty, "entityId");
            
            Guid entityGuid = Helper.GetGuidSafe(entityId);
            if (entityGuid == Guid.Empty)
                throw new ArgumentException(string.Format(Messages.NullEmptyOrInvalidFormat, "entity id", entityId), "entityId");

            string hql = null;
            switch (entityType)
            {
                case EntityType.Dependency:
                    hql = @"Select count(distinct dep.id) From Job as job 
                                Left Join job.dependencies as dep 
                                Where dep.id = :id 
                                And job.Application.Username = :username";
                    break;
                case EntityType.Application:
                    hql = @"Select count(app.Id) From Application app 
                                Where app.Id = :id 
                                And app.Username = :username";
                    break;
                case EntityType.Job:
                    hql = @"Select count(job.Id) From Job job 
                                Where job.Id = :id 
                                And job.Application.Username = :username";
                    break;
                case EntityType.Result:
                    hql = @"Select count(res.Id) From Job as job 
                                Left Join job.results as res 
                                Where res.Id = :id 
                                And job.Application.Username = :username";
                    break;
                default:
                    //do nothing: no one 'owns' an Executor/User/Group etc
                    return false;
            }

            bool isOwner = false;
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadCommitted); //read committed stuff here

                IQuery query = session.CreateQuery(hql);
                query.SetCacheable(true);
                query.SetCacheRegion(OwnershipQueriesRegion); //entity ownership does not change once set. so should be ok to cache.

                query.SetGuid("id", entityGuid);
                query.SetString("username", username);

                long count = 0;
                object result = query.UniqueResult();
                if (result != null)
                    count = (long)result; //play safe

                isOwner = (count == 1);

                //hack: special case for dependency: need to check if it is job scoped / app scoped
                //need to improve this
                if (!isOwner && entityType == EntityType.Dependency)
                {
                    //try once more with app
                    hql = @"Select count(distinct dep.id) From Application as app 
                                Left Join app.dependencies as dep 
                                Where dep.id = :id 
                                And app.Username = :username";
                    
                    query = session.CreateQuery(hql);
                    query.SetGuid("id", entityGuid);
                    query.SetString("username", username);

                    count = 0;
                    result = query.UniqueResult();
                    if (result != null)
                        count = (long)result; //play safe
                    
                    isOwner = (count == 1);
                }

                tr.Commit(); //commit to signal end of transaction
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException(
                    string.Format("Could not check {1} ownership for user '{0}'", entityType, username),
                    hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }

            return isOwner;            
        }
    }
}
