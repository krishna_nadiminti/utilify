﻿using System;
using System.Collections.Generic;
using System.Text;
using NHibernate;
using System.Data;

namespace Utilify.Platform.Manager.Persistence
{
    internal static class SchedulerDao
    {
        internal static IList<Application> GetApplicationsToScheduleBasic()
        {
            ISession session = null;
            DataStore ds = DataStore.GetInstance();
            ITransaction tr = null;
            IList<Application> applications = null;
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadCommitted);

                // Query is:
                // Check for 'ready' apps which have some 'ready' jobs to submit
                // Check that earliest start time has passed
                // Check that either latest start time has not passed OR we have already started executing jobs from this app (otherwise
                //    latest start time becomes like a deadline)
                // Order by Priority then Most recent job start time (this prevents same apps getting scheduled all the time)
                StringBuilder hql = new StringBuilder();

                hql.AppendLine(
                    @"Select app From Application app
                      Where 
                        app.status = :status 
                        And app.Qos.EarliestStartTimeTicks < :timenow
                        And (app.Qos.LatestStartTimeTicks > :timenow Or app.MostRecentJobStartTimeTicks > 0)
                        And app.Id In (Select job.Application.Id From Job job Where job.status = :jobStatus)
                      Order By app.Qos.Priority Desc, 
                               app.MostRecentJobStartTimeTicks Asc, 
                               app.CreationTimeTicks Asc "
                );

                IQuery query = session.CreateQuery(hql.ToString());
                query.SetEnum("status", ApplicationStatus.Ready);
                query.SetEnum("jobStatus", JobStatus.Ready);
                query.SetInt64("timenow", DateTime.UtcNow.Ticks);
                applications = query.List<Application>();

                tr.Commit();
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting applications to schedule.", hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }

            if (applications == null)
                applications = new List<Application>();

            return applications;
        }

        internal static IList<Job> GetJobsToScheduleBasic(Guid applicationId, int count)
        {
            if (applicationId == Guid.Empty)
                throw new ArgumentException("Application Id cannot be empty", "applicationId");
            //if (count <= 0)
            //    throw new ArgumentException("Count must be > 0", "count");

            IList<Job> jobs;
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadCommitted); //read committed stuff

                string hql =
                    @"From Job job 
                      Where job.status = :status 
                        And job.Application.id = :id 
                      Order By job.SubmittedTimeTicks Asc";

                IQuery query = session.CreateQuery(hql);
                query.SetEnum("status", JobStatus.Ready);
                query.SetGuid("id", applicationId);
                query.SetMaxResults(count);
                jobs = query.List<Job>();

                tr.Commit();
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting jobs", hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }
            return jobs;
        }

        internal static IList<Application> GetApplicationsByStatus(ApplicationStatus status) //todoTest: Test if impl
        {
            ISession session = null;
            DataStore ds = DataStore.GetInstance();
            ITransaction tr = null;
            IList<Application> applications = null;
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadUncommitted);

                string hql = "From Application app Where app.status = :status ";

                IQuery query = session.CreateQuery(hql);
                query.SetEnum("status", status);
                applications = query.List<Application>();

                foreach (Application app in applications)
                {
                    NHibernateUtil.Initialize(app.Dependencies); //initialise the dependency collection
                }
                tr.Commit(); //nothing to commit: only a read happening here
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting applications by status: " + status, hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }

            if (applications == null)
                applications = new List<Application>();

            return applications;
        }

        internal static IList<Job> GetJobsByStatus(JobStatus status)
        {
            IList<Job> jobs;
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadCommitted);

                string hql =
                    "From Job job Where job.status = :status ";

                IQuery query = session.CreateQuery(hql);
                query.SetEnum("status", status);
                jobs = query.List<Job>();

                //ugly! can we do this better!?
                //initialise the results collection if needed
                if (status == JobStatus.Completing)
                {
                    foreach (Job job in jobs)
                    {
                        Result[] results = job.GetResults(); //to make sure the results collection is initialized.
                    }
                }

                tr.Commit(); //commit to signal end of transaction
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting jobs", hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;
            }
            return jobs;
        }


        // todoLater: (RemoveMappingsOlderThan(...)) We may want to also remove mappings for executors who are "Offline" and 
        //       have had a mapping sitting around for a while. Could be caused by Executor crashing and not coming back online. 
        //       Could leave this and rely on manual resetting of Jobs.
        internal static int RemoveMappingsOlderThan(TimeSpan timeSpan) //todoTest
        {
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();

            int count = 0;

            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction();

                //Issue 029: perhaps we don't ever need to load this object (and the job+Executor+processors and so on).
                //we could just get the id, and return it? or even better directly delete the mappings in one step
                string hql = @"From JobExecutionMapping mapping 
                                Where (:currentTicks - mapping.CreatedTicks) > :interval 
                                And mapping.Job.status = :status ";

                IQuery query = session.CreateQuery(hql);
                query.SetInt64("currentTicks", DateTime.UtcNow.Ticks);
                query.SetInt64("interval", timeSpan.Ticks);
                query.SetEnum("status", JobStatus.Scheduled); // only get jobs that have 'scheduled' status. leave mappings with other status'

                IList<JobExecutionMapping> mappings = query.List<JobExecutionMapping>();
                foreach (JobExecutionMapping mapping in mappings)
                {
                    //unset the executor, and update status
                    mapping.Job.ExecutionHost = null;
                    mapping.Job.Status = JobStatus.Ready;

                    session.SaveOrUpdate(mapping.Job);
                    session.Delete(mapping);
                }
                count = mappings.Count;
                tr.Commit();
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error retrieving job mappings older than : " + timeSpan, hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }
            
            return count;
        }

        internal static bool CheckJobMappingExists(Guid executorId, Guid jobId) //todoTest
        {
            ISession session = null;
            ITransaction tr = null;
            DataStore ds = DataStore.GetInstance();
            bool mappingExists = false;

            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadCommitted); //read committed stuff here

                string hql = @"Select count(mapping.Id) From JobExecutionMapping mapping 
                                 Where mapping.Executor.id = :executorId 
                                 And mapping.Job.id = :jobId";

                IQuery query = session.CreateQuery(hql);
                query.SetGuid("executorId", executorId);
                query.SetGuid("jobId", jobId);

                long count = (long)query.UniqueResult();
                mappingExists = (count > 0);

                tr.Commit();
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error retrieving job mappings for executor : " + executorId.ToString(), hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }

            return mappingExists;
        }

        internal static long SaveJobMapping(Guid jobId, Guid executorId) //todoTest null mapping/job/exec
        {
            JobExecutionMapping mapping = null;

            if (jobId == Guid.Empty)
                throw new ArgumentException(Messages.ArgumentEmpty, "jobId");
            if (executorId == Guid.Empty)
                throw new ArgumentException(Messages.ArgumentEmpty, "executorId");

            ITransaction tr = null;
            ISession session = null;
            DataStore ds = DataStore.GetInstance();
            try
            {                
                session = ds.OpenSession();
                tr = session.BeginTransaction();

                //Normally we want to retain Mappings even if the Job is being remapped.
                //This allows us to remap a Cancelled or Reset Job, while still allowing the 
                //But what we are doing here is to remove all *previous* mappings, and put in a new one.
                session.Delete("From JobExecutionMapping mapping Where mapping.Job.Id = :jobId",
                    jobId, NHibernateUtil.Guid);

                //get the latest job, executor objects to do everything in a single transaction and avoid stale-state-exception
                Job job = session.Load<Job>(jobId);
                Executor exec = session.Load<Executor>(executorId);

                job.Status = JobStatus.Scheduled;
                job.ScheduledTime = DateTime.UtcNow;
                job.MappedCount += 1;
                job.ExecutionHost = exec;
                mapping = new JobExecutionMapping(job, exec);

                session.SaveOrUpdate(job);
                //We dont need to update the executor at all
                //session.SaveOrUpdate(exec);
                session.SaveOrUpdate(mapping);

                tr.Commit();
            }
            catch (HibernateException ex)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                //job not found stuff would come up as ObjectNotFound, 
                //but executor not found stuff will cause a key violation : 
                //since we're not really using the executor object for anything other than the id itself.
                ObjectNotFoundException ox = ex as ObjectNotFoundException;
                if (ox != null && ox.EntityName == typeof(Job).FullName)
                    throw new JobNotFoundException(Messages.JobNotFound, jobId.ToString());

                throw new PersistenceException(
                    string.Format("Error mapping job {0} to executor {1}", jobId, executorId), ex);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }
            
            if (mapping != null)
                return mapping.Id;
            else
                return 0;
        }

        internal static IList<Job> GetSubmittedJobsFromReadyApplications()
        {
            ISession session = null;
            DataStore ds = DataStore.GetInstance();
            ITransaction tr = null;
            IList<Job> jobs = null;
            try
            {
                session = ds.OpenSession();
                tr = session.BeginTransaction(IsolationLevel.ReadCommitted);

                string hql =
                    @"From Job job
                      Where job.status = :jobStatus
                      And job.Application.status = :status";

                IQuery query = session.CreateQuery(hql);                
                query.SetEnum("status", ApplicationStatus.Ready);
                query.SetEnum("jobStatus", JobStatus.Submitted);
                jobs = query.List<Job>();

                foreach (Job job in jobs)
                {
                    NHibernateUtil.Initialize(job.Dependencies); //initialise the dependency collection.
                }
                tr.Commit();
            }
            catch (HibernateException hx)
            {
                //early release
                ds.RollbackAndDispose(tr);
                tr = null;
                ds.CloseSession(session);
                session = null;

                throw new PersistenceException("Error getting jobs to update to ready.", hx);
            }
            finally
            {
                ds.CloseTransaction(tr);
                tr = null;

                ds.CloseSession(session);
                session = null;
            }

            if (jobs == null)
                jobs = new List<Job>();

            return jobs;
        }
    }
}
