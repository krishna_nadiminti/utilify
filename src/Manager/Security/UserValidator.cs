using System;
using System.Collections.Generic;
using System.Text;
using System.IdentityModel.Selectors;

using System.ServiceModel;
using Utilify.Platform.Manager.Persistence;

namespace Utilify.Platform.Manager.Security
{
    /// <summary>
    /// This class implements the custom Username/Password authentication for the Utilify Platform.
    /// </summary>
    public class UserValidator : UserNamePasswordValidator
    {
        private Logger logger = new Logger();

        public override void Validate(string userName, string password)
        {            
            bool authenticated = false;
            try
            {
                //modified to avoid throwing ArgumentExceptions from this method
                //we now just throw an AuthenticationFault always.
                if (userName != null && userName.Trim().Length > 0
                    && password != null && password.Trim().Length > 0)
                {
                    string hashedPwd = Hasher.ComputeHash(password, HashType.MD5);
                    logger.Debug("XXXXXXXXXXXXXX Calling SecurityDao.AuthenticateUser(). username = {0}",
                        userName);
                    //logger.Debug("Password = {0}, hashed = {1}", password, hashedPwd);
                    // md5 hash pwd and then check with db.
                    authenticated = SecurityDao.AuthenticateUser(userName, Hasher.ComputeHash(password, HashType.MD5));
                }
                else
                {
                    logger.Debug("Null or empty username or password");
                }
            }
            catch (Exception ex)
            {
                logger.Warn("Error authenticating user: {0}", ex.ToString());
            }

            if (!authenticated)
            {
                // This throws an informative fault to the client.
                string msg = "Unknown Username or Incorrect Password";

                throw new FaultException<AuthenticationFault>(
                    new AuthenticationFault(msg), msg, FaultCode.CreateSenderFaultCode(null)
                );
            }
            logger.Debug("XXXXXXXXXXXXXX Successfully Validated User! ");
        }
    }
}
