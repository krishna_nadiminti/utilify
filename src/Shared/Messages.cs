
namespace Utilify.Platform
{
    internal static class Messages
    {
        public const string ApplicationNotFound = "Could not find application.";
        public const string DependencyNotFound = "Could not find dependency.";
        public const string JobNotFound = "Could not find job.";
        public const string ResultNotFound = "Could not find result.";
        public const string ExecutorNotFound = "Could not find executor. Executor may not be registered.";
        public const string UserNotFound = "Could not find user.";
        public const string GroupNotFound = "Could not find group.";
        public const string UserAlreadyExists = "User with provided username already exists.";
        public const string InternalServerError = "Internal server error.";
        public const string InternalServerErrorDescription = "There was an unexpected error processing the client's request. Please contact the administrator for more details.";
        /// <summary>
        /// Takes two params (in format placeholders): {0} parameter name. {1} parameter value
        /// </summary>
        public const string NullOrEmpty = "Value cannot be null or empty";
        public const string ArgumentEmpty = "Value cannot be empty";
        public const string ListEmpty = "List cannot be empty";
        public const string ElementNullOrEmpty = "Elements of the list cannot be null or empty";
        public const string NullEmptyOrInvalidFormat = "The {0} is null, empty or in an invalid format: '{1}'";
    }
}
