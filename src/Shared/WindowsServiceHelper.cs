using System.Collections;
using System.Configuration.Install;
using System.ServiceProcess;

namespace Utilify.Platform
{
    /// <summary>
    /// Summary description for WindowsServiceHelper.
    /// </summary>
    internal static class WindowsServiceHelper
    {
        /// <summary>
        /// Verifies if the Windows service with the given name is installed.
        /// </summary>
        /// <param name="serviceName"></param>
        /// <returns>true if the service is installed properly. false otherwise</returns>
        public static bool CheckServiceInstallation(string serviceName)
        {
            bool exists = false;
            ServiceController sc = null;
            try
            {
                sc = new ServiceController(serviceName);
                sc.Refresh(); //just a dummy call to make sure the service exists.
                exists = true;
            }
            finally
            {
                if (sc != null)
                    sc.Close();
                sc = null;
            }
            return exists;
        }

        /// <summary>
        /// Installs the Windows service with the given "installer" object.
        /// </summary>
        /// <param name="pi"></param>
        /// <param name="pathToService"></param>
        public static void InstallService(Installer pi, string pathToService)
        {
#if !MONO
            TransactedInstaller ti = new TransactedInstaller();
            ti.Installers.Add(pi);
            string[] cmdline = {pathToService};
            InstallContext ctx = new InstallContext("Install.log", cmdline);
            ti.Context = ctx;
            ti.Install(new Hashtable());
#endif
        }

        /// <summary>
        /// UnInstalls the Windows service with the given "installer" object.
        /// </summary>
        /// <param name="pi"></param>
        /// <param name="pathToService"></param>
        public static void UninstallService(Installer pi, string pathToService)
        {
#if !MONO
            TransactedInstaller ti = new TransactedInstaller();
            ti.Installers.Add(pi);
            string[] cmdline = {pathToService};
            InstallContext ctx = new InstallContext("Uninstall.log", cmdline);
            ti.Context = ctx;
            ti.Uninstall(null);
#endif
        }
    }
}