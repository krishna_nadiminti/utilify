using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Threading;

namespace Utilify.Platform
{
    internal static class EventHelper
    {
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "We are executing unknown user code here, so since we need to pass on the event to other handlers, we catch the general Exception here.")]
        private static void InternalRaiseEvent<T>(Delegate eventHandler, object sender, T args, bool genericHandler) 
            where T : EventArgs
        {
            //if no one has subscribed to the event, the handler will be null
            if (eventHandler == null)
                return;
            Delegate[] sinks = eventHandler.GetInvocationList();
            if (sinks == null || sinks.Length == 0)
                return;
            foreach (Delegate sink in sinks)
            {
                try
                {
                    if (genericHandler)
                    {
                        EventHandler<T> handler = (EventHandler<T>)sink;
                        handler(sender, args);
                    }
                    else
                    {
                        sink.DynamicInvoke(sender, args);
                    }
                }
                catch (Exception exception)
                {
                    Trace.TraceError("Exception in event handler : {0}", exception); 
                    //why are we not logging this using the logger? KNA: 18 Dec 2010
                    //because the logger raises events which are raised using this helper class and it will cause an infinite loop if the handler causes errors.
                } //we need to catch exceptions in event handlers since we don't want them to stop execution flow
            }
        }

        //For compatibility with older eventhandler convention
        internal static void RaiseEvent(Delegate eventHandler, object sender, EventArgs args)
        {
            //if no one has subscribed to the event, the handler will be null
            if (eventHandler == null)
                return;

            System.Reflection.ParameterInfo[] parameters = eventHandler.Method.GetParameters();
            if (parameters == null || parameters.Length != 2)
                throw new ArgumentException("Invalid event handler. Expected: a delegate with two parameters: sender, and args");

            if (!parameters[1].GetType().IsSubclassOf(typeof(EventArgs)))
                throw new ArgumentException("Invalid event handler. Expected: a delegate with two parameters: sender, and args");

            InternalRaiseEvent(eventHandler, sender, args, false);
        }

        /// <summary>
        /// Raises an event of type EventHandler&lt;T&gt;
        /// </summary>
        /// <typeparam name="T">T is the type that derives from EventArgs</typeparam>
        internal static void RaiseEvent<T>(EventHandler<T> eventHandler, object sender, T args) where T : EventArgs
        {
            //if no one has subscribed to the event, the handler will be null
            if (eventHandler == null)
                return;
            InternalRaiseEvent<T>(eventHandler, sender, args, true);
        }
       
        /// <summary>
        /// Raises an event of type EventHandler&lt;T&gt; on a background thread asynchronously
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="eventHandler"></param>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        internal static void RaiseEventAsync<T>(EventHandler<T> eventHandler, object sender, T args) where T : EventArgs
        {
            //if no one has subscribed to the event, the handler will be null
            if (eventHandler == null)
                return;

            EventHelperAsyncArgs<T> parameter = new EventHelperAsyncArgs<T>();
            parameter.Sender = sender;
            parameter.EventHandler = eventHandler;
            parameter.Arguments = args;

            Thread asyncEventThread = new Thread(new ParameterizedThreadStart(DoRaiseEvent<T>));
            asyncEventThread.IsBackground = true;
            asyncEventThread.Start(parameter);
        }

        private static void DoRaiseEvent<T>(object eventHelperAsyncArgs) where T : EventArgs
        {
            EventHelperAsyncArgs<T> eha = eventHelperAsyncArgs as EventHelperAsyncArgs<T>;
            RaiseEvent<T>(eha.EventHandler, eha.Sender, eha.Arguments);
        }

        private class EventHelperAsyncArgs<T> where T : EventArgs
        {
            internal EventHelperAsyncArgs(){}

            private object sender;
            internal object Sender
            {
                get { return sender; }
                set { sender = value; }
            }

            private EventHandler<T> eventHandler;
            internal EventHandler<T> EventHandler
            {
                get { return eventHandler; }
                set { eventHandler = value; }
            }

            private T arguments;
            internal T Arguments
            {
                get { return arguments; }
                set { arguments = value; }
            }
        }
    }
}
