using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;

namespace Utilify.Platform
{
    internal static class Helper
    {
        internal static readonly DateTime MinUtcDate = new DateTime(DateTime.MinValue.Ticks, DateTimeKind.Utc);
        internal static readonly DateTime MaxUtcDate = new DateTime(DateTime.MaxValue.Ticks, DateTimeKind.Utc);

        #region Assembly Attribute Accessors

        //todo: replace these with constant strings and use them in the AssemblyInfo attributes!
        //create a global constants class

        private static readonly string companyName = GetCompanyName();
        private static readonly string productName = GetProductName();

        internal static string GetProductName()
        {
            return GetProductName(Assembly.GetCallingAssembly());
        }

        internal static string GetProductName(Assembly asm)
        {
            string name = string.Empty;
            if (asm == null)
                return name;

            AssemblyProductAttribute attr = GetCustomAttribute<AssemblyProductAttribute>(asm);
            if (attr != null)
                name = attr.Product ?? string.Empty;
            else
                name = asm.GetName().Name;

            return name;
        }

        internal static string GetCompanyName()
        {
            string companyName = string.Empty;

            //they're all the same company anyway
            AssemblyCompanyAttribute attr = GetCustomAttribute<AssemblyCompanyAttribute>(Assembly.GetExecutingAssembly());
            if (attr != null)
                companyName = attr.Company ?? string.Empty;

            return companyName;
        }

        internal static string GetAssemblyTitle(Assembly asm)
        {
            string title = string.Empty;

            if (asm == null)
                return title;

            if (asm.ReflectionOnly)
            {
                CustomAttributeData attrData = GetCustomAttributeData<AssemblyTitleAttribute>(asm);
                title = GetCustomAttributePropertyValue(attrData, "Title");
            }
            else
            {
                AssemblyTitleAttribute attr = GetCustomAttribute<AssemblyTitleAttribute>(asm);
                if (attr != null)
                    title = attr.Title ?? string.Empty;
            }
            return title;
        }

        internal static string GetAssemblyVersion(Assembly asm)
        {
            if (asm == null)
                return string.Empty;
            return asm.GetName().Version.ToString();
        }

        internal static string GetAssemblyDescription(Assembly asm)
        {
            string description = string.Empty;
            if (asm == null)
                return description;

            if (asm.ReflectionOnly)
            {
                CustomAttributeData attrData = GetCustomAttributeData<AssemblyDescriptionAttribute>(asm);
                description = GetCustomAttributePropertyValue(attrData, "Description");
            }
            else
            {
                AssemblyDescriptionAttribute attr = GetCustomAttribute<AssemblyDescriptionAttribute>(asm);
                if (attr != null)
                    description = attr.Description ?? string.Empty;
            }
            return description;
        }

        internal static string GetAssemblyCopyright(Assembly asm)
        {
            string copyright = string.Empty;

            if (asm == null)
                return copyright;

            if (asm.ReflectionOnly)
            {
                CustomAttributeData attrData = GetCustomAttributeData<AssemblyCopyrightAttribute>(asm);
                copyright = GetCustomAttributePropertyValue(attrData, "Copyright");
            }
            else
            {
                AssemblyCopyrightAttribute attr = GetCustomAttribute<AssemblyCopyrightAttribute>(asm);
                if (attr != null)
                    copyright = attr.Copyright ?? string.Empty;
            }
            
            return copyright;
        }

        private static string GetCustomAttributePropertyValue(CustomAttributeData cad, string propertyName)
        {
            string value = string.Empty;
            if (cad != null)
            {
                foreach (CustomAttributeTypedArgument carg in cad.ConstructorArguments)
                {
                    if (carg.Value != null)
                        value = carg.Value.ToString();
                        break;
                }
            }
            return value;
        }

        /// <summary>
        /// Gets the first attribute of the specified type 'T'
        /// </summary>
        /// <param name="asm"></param>
        /// <returns></returns>
        private static T GetCustomAttribute<T>(Assembly asm) where T : Attribute
        {
            T attr = default(T);
            if (asm != null && !asm.ReflectionOnly)
            {
                object[] attributes = asm.GetCustomAttributes(typeof(T), false);
                if (attributes != null && attributes.Length > 0)
                    attr = (T)attributes[0];
            }
            return attr;
        }

        /// <summary>
        /// Get custom attribute data for the specified attribute type.
        /// </summary>
        /// <param name="asm"></param>
        /// <returns></returns>
        private static CustomAttributeData GetCustomAttributeData<T>(Assembly asm) where T : Attribute
        {
            CustomAttributeData requiredAttribute = null;

            if (asm != null && asm.ReflectionOnly)
            {
                IList<CustomAttributeData> cAttrs = CustomAttributeData.GetCustomAttributes(asm);
                foreach (CustomAttributeData attr in cAttrs)
                {
                    if (attr.Constructor.DeclaringType.Name.Equals(typeof(T).Name))
                    {
                        requiredAttribute = attr;
                        break;
                    }
                }
            }
            return requiredAttribute;
        }

        #endregion

        internal static bool IsCurrentUserAdmin
        {
            get
            {
                bool isAdmin = false;

                WindowsPrincipal wp = Thread.CurrentPrincipal as WindowsPrincipal;
                isAdmin = (wp != null && wp.IsInRole(WindowsBuiltInRole.Administrator));

                return isAdmin;
            }
        }

        /// <summary>
        /// Gets a copy of the given list.
        /// If the list object is null, an empty array is returned - NOT a null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        internal static T[] GetCopy<T>(IEnumerable<T> list)
        {
            List<T> copy = null;

            if (list != null)
                copy = new List<T>(list);
            else
                copy = new List<T>();

            return copy.ToArray();
        }

        //todoDecide: Remove if not needed.
        /// <summary>
        /// Returns a Utc DateTime instance from the given date. If the given date of kind DateTimeKind.Unspecified, it specifies the kind to be DateTimeKind.Utc.
        /// Otherwise, it converts the given value to universal time.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        internal static DateTime MakeUtc(DateTime value)
        {          
            return value.ToUniversalTime();
        }

        /// <summary>
        /// Returns a guid that corresponds to the specified formatted string value.
        /// If the value is null or empty or in an invalid format, it returns an empty Guid.
        /// </summary>
        /// <param name="value">formatted guid string value</param>
        /// <returns>Guid</returns>
        internal static Guid GetGuidSafe(string value)
        {
            Guid guid = Guid.Empty;
            if (!string.IsNullOrEmpty(value))
            {
                try
                {
                    guid = new Guid(value);
                }
                catch (FormatException) { }
                catch (OverflowException) { }
            }
            return guid;
        }

        /// <summary>
        /// Gets the name of the current CLR.
        /// </summary>
        /// <returns></returns>
        internal static string GetRuntimeName()
        {
            string runtime = ".NET CLR";
            Type t = Type.GetType ("Mono.Runtime");
            if (t != null)
                runtime = "Mono CLR";
            return runtime;
        }

        /// <summary>
        /// Gets the details of the current environment
        /// </summary>
        /// <returns>a string with the environment details</returns>
        internal static string GetEnvironment()
        {
            int clrBitNess = System.IntPtr.Size * 8;

            StringBuilder sb = new StringBuilder();
            Process currentProcess = Process.GetCurrentProcess();
            sb.AppendFormat("Machine: {0} ", Environment.MachineName).AppendLine();
            sb.AppendFormat("Process: {0} [{1}]", currentProcess.ProcessName, currentProcess.Id).AppendLine();
            sb.AppendFormat("Command line: {0}", Environment.CommandLine).AppendLine();
            sb.AppendFormat("System directory: {0}", Environment.SystemDirectory).AppendLine();
            sb.AppendFormat("Current directory: {0}", Environment.CurrentDirectory).AppendLine();
            sb.AppendFormat("User: {0} {1}", Environment.UserName, (Environment.UserInteractive ? "[Interactive]" : string.Empty)).AppendLine();
            sb.AppendFormat("CLR: {0} [{1}] {2}bit", GetRuntimeName(), Environment.Version.ToString(), clrBitNess).AppendLine();
            sb.AppendFormat("OS: {0}", Environment.OSVersion.VersionString).AppendLine();

            //list all env vars
            ListEnvironmentVariables(sb, EnvironmentVariableTarget.Machine).AppendLine();
            ListEnvironmentVariables(sb, EnvironmentVariableTarget.Process).AppendLine();
            ListEnvironmentVariables(sb, EnvironmentVariableTarget.User).AppendLine();

            //list standard folder locations
            ListStandardFolderLocations(sb);

            return sb.ToString();
        }

        private static StringBuilder ListStandardFolderLocations(StringBuilder sb)
        {
            if (sb == null)
                sb = new StringBuilder();
            try
            {
                sb.AppendLine("--- Standard Directory Locations ---");
                Array specialFolders = Enum.GetValues(typeof(Environment.SpecialFolder));            
                string[] folderNames = Enum.GetNames(typeof(Environment.SpecialFolder));
                for (int i = 0; i < specialFolders.Length; i++)
                {
                    string name = folderNames[i];
                    string path = Environment.GetFolderPath((Environment.SpecialFolder)specialFolders.GetValue(i));
                    sb.AppendFormat("{0} = {1}", name, path).AppendLine();
                }
            }
            catch (Exception ex)
            {
                sb.AppendLine("Error getting standard folder locations: " + ex.Message);
            }
            return sb;
        }

        private static StringBuilder ListEnvironmentVariables(StringBuilder sb, EnvironmentVariableTarget target)
        {
            if (sb == null)
                sb = new StringBuilder();

            try
            {
                sb.AppendLine()
                  .AppendFormat("--- {0} Environment ---", target)
                  .AppendLine();

                System.Collections.IDictionary vars = Environment.GetEnvironmentVariables(target);
                if (vars != null && vars.Keys != null)
                {
                    System.Collections.ArrayList list = new System.Collections.ArrayList(vars.Keys);
                    list.Sort(); //we want a sorted list of the env vars
                    foreach (string var in list)
                    {
                        sb.AppendFormat("{0} = {1}", var, vars[var]).AppendLine();
                    }
                }
                else
                {
                    sb.AppendFormat("Could not retrieve {0} environment", target).AppendLine();
                }
            }
            catch (Exception ex)
            {
                sb.AppendFormat("Error getting {0} environment variables: {1}", target, ex.Message).AppendLine();
            }
            return sb;
        }

        /// <summary>
        /// Used to terminate threads.
        /// </summary>
        /// <param name="thread"></param>
        /// <param name="timeout"></param>
        internal static void WaitAndAbort(Thread thread, TimeSpan timeout)
        {
            if (thread != null && thread.IsAlive)
            {
                thread.Join(timeout);
                thread.Abort();
            }
        }

        internal static string GetVersionString(Assembly asm)
        {
            if (asm == null)
                return string.Empty;
            return string.Format("Version: {0}", asm.GetName().Version.ToString());
        }

        internal static string GetFormattedTimeSpan(long milliseconds)
        {
            return GetFormattedTimeSpan(TimeSpan.FromMilliseconds(milliseconds));
        }

        //todo: improve this to show some more detail: like months, days, hours, minutes etc.
        internal static string GetFormattedTimeSpan(TimeSpan span)
        {
            string text = string.Empty;
            string plural = "s";
            
            if (span.Days > 0)
            {
                text = string.Format("{0} day{1}", 
                    span.Days, (span.Days > 1) ? plural : string.Empty);
            }
            else if (span.Hours > 0)
            {
                text = string.Format("{0} hr{1} {2} min{3}", 
                    span.Hours, (span.Hours > 1) ? plural : string.Empty, 
                    span.Minutes, (span.Minutes > 1) ? plural : string.Empty);
            }
            else if (span.Minutes > 0)
            {
                text = string.Format("{0} min{1} {2} sec{3}", 
                    span.Minutes, (span.Minutes > 1) ? plural : string.Empty,
                    span.Seconds, (span.Seconds > 1) ? plural : string.Empty);
            }
            else if (span.Seconds > 0)
            {
                text = string.Format("{0} sec{1}",
                    span.Seconds,
                    (span.Seconds > 1) ? plural : string.Empty);
            }
            else if (span.Milliseconds > 0)
            {
                text = string.Format("{0} ms", span.Milliseconds);
            }

            return text;
        }

        internal static bool AreEqual(byte[] b1, byte[] b2)
        {
            //if both are null - they are equal
            if (b1 == null && b2 == null)
                return true;

            //are they the same instance?
            if (object.ReferenceEquals(b1, b2))
                return true;

            //if only one of them is null, they aren't equal
            if (b1 == null || b2 == null)
                return false;

            if (b1.Length == b2.Length)
            {
                bool areEqual = true;
                for (int i = 0; i < b1.Length; i++)
                {
                    if (b1[i] != b2[i])
                    {
                        areEqual = false;
                        break;
                    }
                }
                return areEqual;
            }

            return false;
        }

        /// <summary>
        /// Compares two date-time instances upto seconds.
        /// </summary>
        /// <param name="d1"></param>
        /// <param name="d2"></param>
        /// <returns></returns>
        internal static bool AreEqual(DateTime d1, DateTime d2)
        {
            return (d1.Date == d2.Date) &&
                (d1.Hour == d2.Hour) &&
                (d1.Minute == d2.Minute) &&
                (d1.Second == d2.Second);
            //removed millisecond check, since that is too fine-grained for us anyway
        }
    }
}
