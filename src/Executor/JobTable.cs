using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Utilify.Platform.Executor
{
    internal class JobTable
    {
        #region Dictionaries for keeping track of Jobs
        
        // NOTE : 2 Dictionaries are used here to improve performance and prevent having to iterate through multiple lists
        // in order to find a specific Job. However we need to ensure that they are kept in sync.

        // Dictionary mapping individual Jobs (Job) to their Status (String/Enum).
        //this is because we don't want any other code to change the status of a job.
        //this 'Job' class is an internal Executor Job, whose status is handled solely by this JobTable
        Dictionary<Job, ExecutionStatus> jobToStatus = 
            new Dictionary<Job, ExecutionStatus>();
        // Dictionary mapping a Status to the list of Jobs having that Status.
        Dictionary<ExecutionStatus,List<Job>> statusToJobs = 
            new Dictionary<ExecutionStatus,List<Job>>();
        
        #endregion

        #region Events

        internal event EventHandler<ExecutionStatusEventArgs> ExecutionStatusChanged;

        #endregion

        /// <summary>
        /// Creates an empty JobTable.
        /// </summary>
        internal JobTable()
        {
            // Initialise the lists for each status.
            foreach (String status in Enum.GetNames(typeof(ExecutionStatus)))
            {
                statusToJobs[(ExecutionStatus)Enum.Parse(typeof(ExecutionStatus), status)] = 
                    new List<Job>();
            }
        }

        /// <summary>
        /// Adds a Job to the JobTable.
        /// </summary>
        /// <remarks>
        /// The 'Add' methods are the only way a Job can be introduced to the JobTable.
        /// </remarks>
        /// <param name="job"></param>
        internal void Add(Job job)
        {
            lock (this)
            {
                Add(job, ExecutionStatus.New);   
            }
        }

        /// <summary>
        /// Adds an Invalid Job to the JobTable. The Job's initial status will be 'Stopped'.
        /// </summary>
        /// <param name="job"></param>
        internal void AddInvalid(Job job)
        {
            lock (this)
            {
                Add(job, ExecutionStatus.Stopped);   
            }
        }

        private void Add(Job job, ExecutionStatus status)
        {
            if (job == null)
                throw new ArgumentNullException("job");

            lock (this)
            {
                if (jobToStatus.ContainsKey(job))
                    throw new ArgumentException("Cannot Add Job. Job already exists.", "job");

                // Should be atomic?
                jobToStatus.Add(job, status);
                statusToJobs[status].Add(job);

                // Raise the Event.
                OnExecutionStatusChanged(status, job);
            }
        }

        /// <summary>
        /// Changes the Status of a Job.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="newStatus"></param>
        internal void ChangeStatus(Job job, ExecutionStatus newStatus)
        {
            if (job == null)
                throw new ArgumentNullException("job");

            lock (this)
            {
                if (!jobToStatus.ContainsKey(job)) // probably not here because the job got cancelled. but we dont really need to blow up.
                    throw new ArgumentException("Cannot update Job Status. Job does not exist.");

                ExecutionStatus oldStatus = jobToStatus[job];

                if (!IsValidStatusChange(oldStatus, newStatus))
                    throw new InvalidTransitionException(String.Format("Cannot update Job Status. Job cannot transition from {0} to {1}.", oldStatus, newStatus));

                // Should be atomic
                statusToJobs[oldStatus].Remove(job);
                statusToJobs[newStatus].Add(job);
                jobToStatus[job] = newStatus;

                // Raise the Event.
                OnExecutionStatusChanged(newStatus, job);
            }
        }

/*
        internal void CancelJob(String jobId)
        {
            if (!String.IsNullOrEmpty(jobId))
                throw new ArgumentNullException("jobId");

            List<Job> jobs = new List<Job>(jobToStatus.Keys);
            foreach (Job job in jobs)
	        {
                if (job.JobId.Equals(jobId))
                {
                    statusToJobs[oldStatus].Remove(job);
                    jobToStatus.Remove(job);
                    job.IsCancelled = true;
                }
	        }
        }
*/

        internal void CancelJobs(List<string> jobIds)
        {
            if (jobIds == null)
                throw new ArgumentNullException("jobIds");

            lock (this)
            {
                List<Job> jobs = new List<Job>(jobToStatus.Keys);
                ExecutionStatus status;
                foreach (Job job in jobs)
                {
                    if (jobIds.Contains(job.JobId))
                    {
                        status = jobToStatus[job];
                        statusToJobs[status].Remove(job);
                        jobToStatus.Remove(job);
                        job.IsCancelled = true;
                        // Raise the Event.
                        OnExecutionStatusChanged(ExecutionStatus.Cancelled, job);
                    }
                }
            }
        }

        /// <summary>
        /// Checks whether the given Status change is a valid one.
        /// </summary>
        /// <param name="oldStatus"></param>
        /// <param name="newStatus"></param>
        /// <returns></returns>
        private bool IsValidStatusChange(ExecutionStatus oldStatus, ExecutionStatus newStatus)
        {
            //todoFix implement (IsValidStatusChange(...))
            if (oldStatus == ExecutionStatus.Cancelled || oldStatus == ExecutionStatus.Stopped)
                return false;

            return true;
        }

        /// <summary>
        /// Removes a Job.
        /// </summary>
        /// <remarks>
        /// This is the only way to remove a Job from the JobTable.
        /// </remarks>
        /// <param name="job"></param>
        internal void Remove(Job job)
        {
            if (job == null)
                throw new ArgumentNullException("job");
            lock (this)
            {
                if (!jobToStatus.ContainsKey(job))
                    throw new ArgumentException("Cannot update Job Status. Job does not exist.");

                ExecutionStatus status = jobToStatus[job];

                // Should be atomic.
                statusToJobs[status].Remove(job);
                jobToStatus.Remove(job);                
            }
        }

        /// <summary>
        /// Returns all the Jobs with a given Status.
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        internal Job[] GetJobs(ExecutionStatus status)
        {
            lock (this)
            {
                return statusToJobs[status].ToArray();                
            }
        }

        /// <summary>
        /// Returns the job with the given Id and status, if it exists.
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        internal Job GetJob(String jobId, ExecutionStatus status)
        {
            Job jobToReturn = null;
            lock (this)
            {
                List<Job> jobs = statusToJobs[status];
                foreach (Job job in jobs)
                {
                    if (job.JobId.Equals(jobId))
                        jobToReturn = job;
                }
                return jobToReturn;                
            }
        }

        /// <summary>
        /// Gets all Jobs.
        /// </summary>
        internal Job[] Jobs
        {
            get
            {
                lock (this)
                {
                    List<Job> jobs = new List<Job>(jobToStatus.Keys);
                    return jobs.ToArray();                    
                }
            }
        }

        /// <summary>
        /// Returns the count of Jobs with a given Status.
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        internal int GetCount(ExecutionStatus status)
        {
            lock (this)
            {
                //the statusToJobs is initialised with empty lists for each status.
                //so we can directly look it up here 
                return statusToJobs[status].Count;
            }
        }

        internal bool HasRunningJobs(ClientPlatform platform)
        {
            ReadOnlyCollection<Job> runningJobs = null;
            lock (this)
            {
                runningJobs = statusToJobs[ExecutionStatus.Running].AsReadOnly();
            }
            if (runningJobs != null)
            {
                foreach (var job in runningJobs)
                {
                    if (job.ClientPlatform == platform)
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets the total count of Jobs for all Statuses.
        /// </summary>
        internal int Count
        {
            get 
            {
                lock (this)
                {
                    return jobToStatus.Count;                     
                }
            }
        }

        /// <summary>
        /// Returns the Status for a given Job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        internal ExecutionStatus GetStatus(Job job)
        {
            lock (this)
            {
                return jobToStatus[job];                
            }
        }

        protected virtual void OnExecutionStatusChanged(ExecutionStatus status, Job job)
        {
            ExecutionStatusEventArgs args = new ExecutionStatusEventArgs(status, job);
            EventHelper.RaiseEventAsync<ExecutionStatusEventArgs>(ExecutionStatusChanged, this, args);
        }
    }

    /// <summary>
    /// Event args passed when a Job changes status.
    /// </summary>
    internal class ExecutionStatusEventArgs : EventArgs
    {
        private Job job;
        private ExecutionStatus status;

        internal ExecutionStatusEventArgs()
            : base()
        { }

        internal ExecutionStatusEventArgs(ExecutionStatus status, Job job)
            : base()
        {
            this.job = job;
            this.status = status;
        }

        internal Job Job
        {
            get { return job; }
            set { job = value; }
        }

        internal ExecutionStatus Status
        {
            get { return status; }
            set { status = value; }
        }
    }

    internal enum ExecutionStatus
    {
        New = 0,
        Preparing = 1,
        Ready = 2,
        Running = 3,
        Stopped = 4,
        Cancelled = 5
    }
}
