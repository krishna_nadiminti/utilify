using System;

namespace Utilify.Platform.Executor
{
    internal class Job
    {
        private string jobId;
        private string applicationId;
        private byte[] initialJobInstance;
        private DependencyInfo[] dependencies;
        private ResultInfo[] expectedResults;
        private JobType jobType;
        private ClientPlatform clientPlatform;

        private string jobDirectory;
        private string applicationDirectory;
        private byte[] finalJobInstance;
        private byte[] executionExceptionInstance;
        private StringAppender executionLog;
        private StringAppender executionError;
        private bool cancelled = false;
        private int failureCount;

        private long cpuTime; //the total cpu time taken by the job on the executor

        internal Job(JobSubmissionInfo jobSubmissionInfo)
        {
            ValidationHelper.CheckNull(jobSubmissionInfo, "jobSubmissionInfo");

            this.jobId = jobSubmissionInfo.JobId;
            this.applicationId = jobSubmissionInfo.ApplicationId;
            this.initialJobInstance = jobSubmissionInfo.GetJobInstance();
            this.dependencies = jobSubmissionInfo.GetDependencies();
            this.expectedResults = jobSubmissionInfo.GetExpectedResults();
            this.jobType = jobSubmissionInfo.JobType;
            this.clientPlatform = jobSubmissionInfo.ClientPlatform;

            this.jobDirectory = PathHelper.GetJobDirectory(this.applicationId, this.jobId);
            this.applicationDirectory = PathHelper.GetApplicationDirectory(this.applicationId);
            this.finalJobInstance = null;
            this.executionExceptionInstance = null;
            this.executionLog = new StringAppender();
            this.executionError = new StringAppender();

            this.failureCount = 0;
        }

        internal String JobId
        {
            get { return jobId; }
        }

        internal String ApplicationId
        {
            get { return applicationId; }
        }

        internal JobType JobType
        {
            get { return jobType; }
        }

        internal ClientPlatform ClientPlatform
        {
            get { return clientPlatform; }
        }

        internal String JobDirectory
        {
            get { return jobDirectory; }
        }

        internal String ApplicationDirectory
        {
            get { return applicationDirectory; }
        }

        internal StringAppender ExecutionLog
        {
            get { return executionLog; }
            //private set
            //{
            //    if (value == null)
            //        throw new ArgumentNullException("executionLog");

            //    executionLog = value;
            //}
        }

        internal StringAppender ExecutionError
        {
            get { return executionError; }
            //private set
            //{
            //    if (value == null)
            //        throw new ArgumentNullException("executionError");

            //    executionError = value;
            //}
        }

        internal byte[] InitialJobInstance
        {
            get { return initialJobInstance; }
        }

        internal byte[] FinalJobInstance
        {
            get { return finalJobInstance; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("finalJobInstance");

                if (finalJobInstance != null)
                    throw new ArgumentException("The final job instance can only be set once.", "finalJobInstance");

                finalJobInstance = value;
            }
        }

        internal byte[] ExecutionExceptionInstance
        {
            get { return executionExceptionInstance; }
            set { executionExceptionInstance = value; }
        }

        //todoFix: need to lock this. this may have threading problems
        internal bool IsCancelled
        {
            get { return cancelled; }
            set { cancelled = value; }
        }

        internal DependencyInfo[] GetDependencies()
        {
            return this.dependencies;
        }

        internal ResultInfo[] GetExpectedResults()
        {
            return this.expectedResults;
        }

        internal long CpuTime
        {
            get { return cpuTime; }
            set 
            {
                if (value < 0)
                    throw new ArgumentException("Value cannot be negative: cpuTime");

                cpuTime = value; 
            }
        }

        internal int FailureCount
        {
            get { return failureCount; }
            set { failureCount = value; }
        }

        internal JobCompletionInfo ToJobCompletionInfo()
        {
            JobCompletionInfo jobCompletionInfo =
                new JobCompletionInfo(
                jobId,
                applicationId,
                finalJobInstance,
                executionExceptionInstance,
                executionLog.ToString(),
                executionError.ToString(), 
                cpuTime);

            return jobCompletionInfo;
        }
    }
}
