using System;
using System.Collections.Generic;
using System.Text;
//using Utilify.Framework;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Xml;
using System.Reflection;
using Utilify.Platform;

namespace Utilify.Internal.Tools
{
    class Program
    {
        private const string GacRunnerBatLocation = @"RunGacUtil.bat";
        private const string GacListHeader = @"The Global Assembly Cache contains the following assemblies:";

        private const string MsPublicToken = @"PublicKeyToken=b77a5c561934e089";
        private const string MsPublicToken1 = @"PublicKeyToken=b03f5f7f11d50a3a";

        private const string CorAssembly = @"mscorlib";
        private const string SystemAssembly = @"System";
        private const string CLRVersion = @"Version=2.0.0.0";
        private const string procArch = @", processorArchitecture=";
        private const string PlatformAssembliesFile = @"PlatformAssemblies.xml";

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Running gacUtil using '{0}'", GacRunnerBatLocation);

                Process gacUtil = new Process();
                gacUtil.StartInfo.UseShellExecute = false;
                gacUtil.StartInfo.FileName = GacRunnerBatLocation;
                gacUtil.StartInfo.WorkingDirectory = Environment.CurrentDirectory;
                gacUtil.StartInfo.Arguments = "gacList.txt";
                gacUtil.Start();
                gacUtil.WaitForExit();

                Console.WriteLine("Parsing GacUtil output from gacList.txt");

                List<string> assemblyList = new List<string>();
                using (StreamReader sr = new StreamReader("gacList.Txt"))
                {
                    string s = null;
                    while ((s = sr.ReadLine()) != null)
                    {
                        if (s == GacListHeader)
                            continue;

                        if ((s.Contains(MsPublicToken) || s.Contains(MsPublicToken1)) && 
                            (s.Contains(SystemAssembly) || s.Contains(CorAssembly)) && 
                            s.Contains(CLRVersion))
                        {
                            int index = s.IndexOf(procArch);
                            if (index > 0)
                                s = s.Substring(0, index);
                            assemblyList.Add(s.Trim());
                        }
                    }
                }

                assemblyList.Sort();

                //add utilify platform assemblies
                List<string> utilifyAssemblies = GetUtilifyPlatformAssemblies(assemblyList);
                utilifyAssemblies.Sort();
                assemblyList.AddRange(utilifyAssemblies);

                Console.WriteLine("Creating platform assemblies xml file ...");
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateXmlDeclaration("1.0", "UTF-8", "yes"));
                doc.AppendChild(doc.CreateComment("Attribute 'name' for element 'assembly' is the FullName returned by Assembly.FullName propertyName"));
                XmlElement assemblies = doc.CreateElement("assemblies");
                foreach (string assembly in assemblyList)
                {
                    XmlElement asm = doc.CreateElement("assembly");
                    asm.Attributes.Append(doc.CreateAttribute("name"));
                    asm.Attributes["name"].Value = assembly;
                    assemblies.AppendChild(asm);
                }
                doc.AppendChild(assemblies);
                doc.Save(PlatformAssembliesFile);
                Console.WriteLine("Saved assembly list to '{0}'. Press any key to end...", PlatformAssembliesFile);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadKey();
            }
        }

        private static List<string> GetUtilifyPlatformAssemblies(List<string> ignoreList)
        {
            List<string> deps = new List<string>();
            
            List<string> frameworkList = DetectDependencies(typeof(Logger),
                ignoreList);
            foreach (string asm in frameworkList)
            {
                if (!ignoreList.Contains(asm) &&
                    !deps.Contains(asm))
                {
                    deps.Add(asm);
                }
            }

            List<string> executorList = DetectDependencies(Type.GetType("Utilify.Platform.Executor.ExecutorHost"),
                ignoreList);
            foreach (string asm in executorList)
            {
                if (!ignoreList.Contains(asm) &&
                    !deps.Contains(asm))
                {
                    deps.Add(asm);
                }
            }

            List<string> managerList = DetectDependencies(Type.GetType("Utilify.Platform.Manager.ManagerHost"),
                ignoreList);
            foreach (string asm in managerList)
            {
                if (!ignoreList.Contains(asm) &&
                    !deps.Contains(asm))
                {
                    deps.Add(asm);
                }
            }

            return deps;
        }

        private static List<string> DetectDependencies(Type type, List<string> ignoreList)
        {
            List<string> dependencies = new List<string>();

            //list of asssembly full names that need to be checked
            Stack<string> assembliesToCheck = new Stack<string>();
            assembliesToCheck.Push(type.Assembly.FullName);
            
            while (assembliesToCheck.Count > 0)
            {
                string currentAssemblyFullName = assembliesToCheck.Pop();

                //double check to make sure we don't process the same assembly twice
                if (dependencies.Contains(currentAssemblyFullName))
                    continue;

                //check if it is a standard assembly
                if (ignoreList.Contains(currentAssemblyFullName))
                    continue;

                dependencies.Add(currentAssemblyFullName);

                Assembly currentAssembly = Assembly.ReflectionOnlyLoad(currentAssemblyFullName);
                AssemblyName[] references = currentAssembly.GetReferencedAssemblies();
                foreach (AssemblyName asm in references)
                {
                    string referenceFullName = asm.FullName;
                    if (!dependencies.Contains(referenceFullName) &&
                        !ignoreList.Contains(referenceFullName) &&
                        !assembliesToCheck.Contains(referenceFullName))
                    {
                        //push it onto the stack for processing in the next round
                        assembliesToCheck.Push(referenceFullName);
                    }
                }
            }

            return dependencies;
        }

    }
}
